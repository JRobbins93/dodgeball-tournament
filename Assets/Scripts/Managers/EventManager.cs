﻿using System;
using System.Collections.Generic;
using UnityEngine;

//This script is responsible for sending events to other scripts that are registered as listeners so that they may respond accordingly
//This script is intended to be persistent across the whole game and should be added to the _Init scene
public class EventManager : MonoBehaviour {

    private Dictionary<string, List<Action>> m_callbacks;

    private void Awake()
    {
        m_callbacks = new Dictionary<string, List<Action>>();
    }

	//Registers the provided method to be called whenever the event is fired
    public void AddListener(string eventName, Action callback)
    {
		//If no listeners are currently registered for the given event, add a list with our provided callback to the dictionary under the event name
        if (!m_callbacks.ContainsKey(eventName))
        {
			List<Action> newEntry = new List<Action>();
			newEntry.Add(callback);
            m_callbacks.Add(eventName, newEntry);
        }
		//Otherwise add the provided method to the existing dictionary entry
		else
		{
			List<Action> entry = m_callbacks[eventName];
			entry.Add(callback);
        }
    }

	//Removes the provided method from the registered methods to be called when the given event is fired
    public void RemoveListener(string eventName, Action callback)
    {
		List<Action> entry = m_callbacks[eventName];
		entry.Remove(callback);

		//If there are no more registered callbacks, we can remove the event name from our dictionary
		if(entry.Count == 0)
		{
			m_callbacks.Remove(eventName);
		}
    }

	//Calls all callback methods that registered to listen for the given event to be fired
    public void SendEvent(string eventName)
    {
        if (m_callbacks.ContainsKey(eventName))
        {
			List<Action> entry = m_callbacks[eventName];

			//Iterate through all of the registered methods, triggering each one
			for(int actionIndex = 0; actionIndex < entry.Count; actionIndex++)
			{
				Action currentAction = entry[actionIndex];
				currentAction();
			}
        }
    }
}
