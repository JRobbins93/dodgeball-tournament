﻿using UnityEngine;
using UnityEngine.SceneManagement;

//This script coordinates all of the music played throughout the game
//It is responsible for fading music out/in upon scene change to avoid a jarring transition
//This script, therefore, is intended to be persistent across the whole game and should be added to the _Init scene
public class MusicManager : MonoBehaviour {

	[SerializeField]
	private EventManager m_eventManager = null;

	//Reference to the AudioSource that plays all of the music
    [SerializeField]
    private AudioSource m_audioSource = null;

	//References to the AudioClips containing the music for each scene in the game
    [SerializeField]
    private AudioClip m_menuSceneMusicAudioClip = null;
    [SerializeField]
    private AudioClip m_tutorialSceneMusicAudioClip = null;
    [SerializeField]
    private AudioClip m_gameSceneMusicAudioClip = null;

	//Record of when the last scene change was, used in volume fading calculations
    private float m_previousSceneChangeRealTime;
	private string m_previousSceneName;

	//Reference to the AudioClip to play once the current one has faded out
    private AudioClip m_nextSceneAudioClip;

    private void Start()
    {
		m_eventManager.AddListener(Constants.EventNames.ON_SCENE_LOADED, OnSceneLoaded);
	}

    private void Update()
    {
		//There is another AudioClip queued, need to fade out the current one
        if (m_nextSceneAudioClip != null)
        {
			//Scale volume by the time since the scene loaded
            float timeSinceSceneLoad = Time.realtimeSinceStartup - m_previousSceneChangeRealTime;

			//Volume is normalized, so move it between 1 and 0 to fade out
			m_audioSource.volume = Constants.Audio.MUSIC_MAX_VOLUME - (timeSinceSceneLoad / Constants.Audio.MUSIC_FADE_OUT_TIME_ON_SCENE_LOAD);

            if(m_audioSource.volume <= 0.0f)
            {
				//Fade out complete, prepare the next AudioClip for fade in
                m_audioSource.clip = m_nextSceneAudioClip;
                m_audioSource.Play();
                m_nextSceneAudioClip = null;
            }
        }
        else if(m_audioSource.volume < Constants.Audio.MUSIC_MAX_VOLUME)
        {
			//Scale volume by the time since the scene loaded
			float timeSinceFadeOut = Time.realtimeSinceStartup - m_previousSceneChangeRealTime - Constants.Audio.MUSIC_FADE_OUT_TIME_ON_SCENE_LOAD;

			//Volume is normalized, so move it between 0 and 1 to fade in
            m_audioSource.volume = timeSinceFadeOut / Constants.Audio.MUSIC_FADE_IN_TIME_ON_SCENE_LOAD;
        }
    }

	private void OnDestroy()
	{
		m_eventManager.RemoveListener(Constants.EventNames.ON_SCENE_LOADED, OnSceneLoaded);
	}


	//Event callback triggered upon loading of a scene, sets the queued AudioClip to be the correct music track for the newly loaded scene
	private void OnSceneLoaded()
	{
		string activeSceneName = SceneManager.GetActiveScene().name;

		if(activeSceneName == m_previousSceneName)
		{
			//When restarting a game from within the Game scene, rather than entering from the Menu scene, we don't want to change the music
			return;
		}

		//Record time of scene change for use in volume lerping
		m_previousSceneChangeRealTime = Time.realtimeSinceStartup;

		switch (activeSceneName)
		{
			case Constants.SceneNames.MENU:
				m_nextSceneAudioClip = m_menuSceneMusicAudioClip;
				break;
			case Constants.SceneNames.TUTORIAL:
				m_nextSceneAudioClip = m_tutorialSceneMusicAudioClip;
				break;
			case Constants.SceneNames.GAME:
				m_nextSceneAudioClip = m_gameSceneMusicAudioClip;
				break;
		}

		//Store new scene name for future checks
		m_previousSceneName = activeSceneName;
	}
}
