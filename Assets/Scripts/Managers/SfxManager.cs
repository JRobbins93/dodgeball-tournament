﻿using UnityEngine;

//This script coordinates all of the sound effects played throughout the game
//This script is intended to be persistent across the whole game and should be added to the _Init scene
public class SfxManager : MonoBehaviour {

    [SerializeField]
    private EventManager m_eventManager = null;

	//Reference to the AudioSource that plays all of the SFX
	[SerializeField]
    private AudioSource m_audioSource = null;

	//References to the AudioClips containing the SFX for the game
	[SerializeField]
	private AudioClip m_characterEliminatedAudioClip = null;
	[SerializeField]
	private AudioClip m_gameEndedAudioClip = null;
	[SerializeField]
	private AudioClip m_menuActionAudioClip = null;

    private void Start()
    {
		//Listen to events so that SFXs can be triggered in response
		m_eventManager.AddListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		m_eventManager.AddListener(Constants.EventNames.ON_GAME_ENDED, OnGameEnded);
		m_eventManager.AddListener(Constants.EventNames.ON_MENU_ACTION, OnMenuAction);
    }

	private void OnDestroy()
	{
		m_eventManager.RemoveListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		m_eventManager.RemoveListener(Constants.EventNames.ON_GAME_ENDED, OnGameEnded);
		m_eventManager.RemoveListener(Constants.EventNames.ON_MENU_ACTION, OnMenuAction);
	}

	//Plays the provided AudioClip
    private void PlayAudioClip(AudioClip audioClip)
    {
        m_audioSource.PlayOneShot(audioClip);
    }

	//Plays the SFX for when a Character is eliminated
	private void OnCharacterEliminated()
	{
		PlayAudioClip(m_characterEliminatedAudioClip);
	}

	//Plays the SFX for when a Game ends
	private void OnGameEnded()
	{
		PlayAudioClip(m_gameEndedAudioClip);
	}

	//Plays the SFX for when a menu transition occurs
	private void OnMenuAction()
	{
		PlayAudioClip(m_menuActionAudioClip);
	}
}
