﻿using System.Collections.Generic;
using UnityEngine;

//This script is responsible for providing AICharacterCommandIssuers with new targets
public class AICoordinator {

	//References to all Characters and Balls in the scene, used in making AI decisions
	private List<Character> m_characters;
	private List<Ball> m_balls;

	//To avoid sending all Characters from one Team after the same Ball, keep track of those which have already been used as targets for other Characters
	private List<Ball> m_spokenForBalls = new List<Ball>();

	public void SetCharacters(List<Character> characters)
	{
		m_characters = characters;
	}

	public void SetBalls(List<Ball> balls)
	{
		m_balls = balls;
	}

	//Remove a ball from the list of 'spoken for' balls, allowing it to be targetted for pickup by other Characters
	public void ClearSpokenForBall(Ball ball)
	{
		m_spokenForBalls.Remove(ball);
	}

	//Calculates what actions an AI agent should perform next
	public void UpdateTargets(AICharacterCommandIssuer agent)
	{
		Character agentCharacter = agent.GetCharacter();
		Vector2 agentCharacterPosition = agentCharacter.transform.position;

		Ball heldBall = agentCharacter.GetHeldBall();

		Ball targetBall = agent.GetTargetBallToPickup();
		Character targetCharacter = agent.GetTargetCharacterToThrowAt();

		//The AI agents decision making process is roughly:
		//"If I've just picked up a Ball, I need to move to a random spot and then throw the Ball at an opposing Character"
		//"If I'm not holding a Ball, I need to find one to pick up and move towards it"
		//"If there are no Balls I can pick up, I will move to a random location while I wait for one to become available"
		if (heldBall != null)
		{
			if(heldBall.IsInPlay())
			{
				//Need to find target to throw ball at and destination to move to
				targetCharacter = GetNewTargetCharacterToThrowAt(agentCharacter);

				if (targetCharacter != null)
				{
					//Rather than having the AI agent throw their Ball at the new target Character as soon as they pick it up,
					//instead have them move to a random position before throwing
					agent.SetTargetPosition(GetNewTargetPosition(agentCharacter));
					agent.SetTargetCharacterToThrowAt(targetCharacter);
					return;
				}
			}
			else
			{
				//Need to move to back wall of the agent's side of the court to put their held Ball into play
                if(agentCharacter.GetTeamIndex() == 0)
                {
                    agentCharacterPosition.x = Constants.EntityDimensions.COURT_MIN_X;
                }
				else
				{
                    agentCharacterPosition.x = Constants.EntityDimensions.COURT_MAX_X;
                }

				agent.SetTargetPosition(agentCharacterPosition);
				return;
			}
		}
		else
		{
			//Need to find ball to pick up and set destination to that ball's position
			targetBall = GetNewTargetBallToPickup(agentCharacter);

			if (targetBall != null)
			{
				agent.SetTargetBallToPickup(targetBall);
				return;
			}
			else
			{
				//There are no target Characters to throw a Ball at and no Balls to pick up
				//Need to pick random target destination until a new taret becomes available
				agent.SetTargetPosition(GetNewTargetPosition(agentCharacter));
			}
		}
	}

	//Finds a Ball (if any) that the provided Character is able to pick up
	private Ball GetNewTargetBallToPickup(Character character)
	{
		List<Ball> viableBalls = new List<Ball>();

		//Iterate across all balls in the scene
		for(int ballIndex = 0; ballIndex < m_balls.Count; ballIndex++)
		{
			Ball currentBall = m_balls[ballIndex];

			//If a ball is not in play, it can only be picked up a Character on a certain team
			//If the ball is in play, it can be picked up as long as it is on the floor
			//We don't consider a Ball viable to be picked up if it is in our list of 'spoken for' Balls
			if (currentBall.IsOnFloor() && (currentBall.IsInPlay() || currentBall.GetInitialPickupTeamIndex() == character.GetTeamIndex()) && !m_spokenForBalls.Contains(currentBall) && currentBall.transform.position.x * character.transform.position.x >= 0)
			{
				viableBalls.Add(currentBall);
			}
		}

		//Perfectly acceptable that there are no Balls the Character can pick up
		if(viableBalls.Count == 0)
		{
			return null;
		}
		else
		{
			//Select a random Ball from the list of all viable ones
			Ball newTargetBall = viableBalls[Random.Range(0, viableBalls.Count - 1)];

			//Don't want to let another AI Character attempt to pick up the same Ball, so add it to the list of 'spoken for' ones
			m_spokenForBalls.Add(newTargetBall);

			return newTargetBall;
		}
	}

	//Finds an opposing Character that the AI agent can throw the ball they are holding at
	private Character GetNewTargetCharacterToThrowAt(Character character)
	{
		List<Character> viableCharacters = new List<Character>();

		//Iterate through all Characters in the game, finding the ones who have not been eliminated and are on the other team
		for (int characterIndex = 0; characterIndex < m_characters.Count; characterIndex++)
		{
			Character currentCharacter = m_characters[characterIndex];

			if (currentCharacter.IsInPlay() && currentCharacter.GetTeamIndex() != character.GetTeamIndex())
			{
				viableCharacters.Add(currentCharacter);
			}
		}

		//If there are no viable Characters to throw at then it should mean that the game has ended and the agent's team has won
		if (viableCharacters.Count == 0)
		{
			return null;
		}
		else
		{
			//Return a random Character for the list of viable ones
			return viableCharacters[Random.Range(0, viableCharacters.Count - 1)];
		}
	}

	//Gets a random position with the agent's side of the court
	private Vector2 GetNewTargetPosition(Character character)
	{
        float x = Random.Range(Constants.EntityDimensions.CHARACTER_MIN_X[character.GetTeamIndex()], Constants.EntityDimensions.CHARACTER_MAX_X[character.GetTeamIndex()]);
        float y = Random.Range(Constants.EntityDimensions.CHARACTER_MIN_Y, Constants.EntityDimensions.CHARACTER_MAX_Y);
                
        return new Vector2(x, y);
	}
}
