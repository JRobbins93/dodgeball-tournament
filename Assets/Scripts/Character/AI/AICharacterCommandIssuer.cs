﻿using UnityEngine;

//This script consults with an AI coordinator to determine what actions an AI agent should be perfoming
//It then returns these actions in the form of a CharacterCommands object which can then be performed by the associated Character
public class AICharacterCommandIssuer : ICharacterCommandIssuer
{
	private AICoordinator m_coordinator;
	private Character m_character;

	private Vector2 m_targetPosition;
	private Ball m_targetBallToPickup;
	private Character m_targetCharacterToThrowAt;

	public Character GetCharacter()
	{
		return m_character;
	}

	public Vector2 GetTargetPosition()
	{
		return m_targetPosition;
	}

	public Ball GetTargetBallToPickup()
	{
		return m_targetBallToPickup;
	}

	public Character GetTargetCharacterToThrowAt()
	{
		return m_targetCharacterToThrowAt;
	}

	public void SetTargetPosition(Vector2 targetPosition)
	{
		m_targetPosition = targetPosition;
	}

	public void SetTargetBallToPickup(Ball ballToPickup)
	{
		m_targetBallToPickup = ballToPickup;
		//Meed to also set the target position so the agent will move towards the Ball
		m_targetPosition = ballToPickup.transform.position;
	}

	public void SetTargetCharacterToThrowAt(Character characterToThrowAt)
	{
		m_targetCharacterToThrowAt = characterToThrowAt;
	}

	public AICharacterCommandIssuer(Character character, AICoordinator coordinator)
	{
		m_character = character;
		m_coordinator = coordinator;

		//Default the target position to be the Character's starting position
		m_targetPosition = m_character.transform.position;
	}

	public CharacterCommands GetCommands()
	{
		Vector2 characterPosition = (Vector2)m_character.transform.position;
		Vector2 toTargetPosition = m_targetPosition - characterPosition;
		float distanceToTargetPosition = toTargetPosition.magnitude;

		CharacterCommands commands = new CharacterCommands();

		//Always move towards the target position
		commands.m_movement = toTargetPosition;

		//If agent is not close enough to their target position, then all they need to do is keep moving towards it
		if(distanceToTargetPosition < Constants.AI.TARGET_POSITION_PROXIMITY_TOLERANCE)
		{
			//Agent is close enough to the target Ball that they can pick it up
			if(m_targetBallToPickup != null)
			{
				commands.m_pickup = true;
				//We can stop considering the Ball as 'spoken for' so that it can be picked up by another AI Character next time it is on the floor
				m_coordinator.ClearSpokenForBall(m_targetBallToPickup);
				m_targetBallToPickup = null;
			}

			//Agent has reached the random position they needed to move to before throwing their Ball at their target Character
			if(m_targetCharacterToThrowAt != null)
			{
				Vector2 toTargetCharacter = (Vector2)m_targetCharacterToThrowAt.transform.position - characterPosition;

				//Don't want the AI opponents to be too accurate, as this would make the game very difficult for a human player.
				//We therefore apply a random angle to the aim vector to add some fallibility
				float randomAngle = Random.Range(Constants.AI.MIN_ERROR_ANGLE, Constants.AI.MAX_ERROR_ANGLE);

				commands.m_aim = Quaternion.AngleAxis(randomAngle, Vector3.forward) * toTargetCharacter;

				commands.m_throw = true;
				m_targetCharacterToThrowAt = null;
			}
		}

		//The Character will need a new target if any other the following occur:
		//	Their target Ball has been picked up by someone else
		//	Their target Character has been eliminated
		//	They have reached their target position
		if (m_targetBallToPickup != null && !m_targetBallToPickup.IsOnFloor() ||
			m_targetCharacterToThrowAt != null && !m_targetCharacterToThrowAt.IsInPlay() ||
			distanceToTargetPosition < Constants.AI.TARGET_POSITION_PROXIMITY_TOLERANCE)
		{
			m_coordinator.UpdateTargets(this);

			//Since we have new targets, will need to refresh the stored direction and distance
			toTargetPosition = m_targetPosition - characterPosition;
			distanceToTargetPosition = toTargetPosition.magnitude;
		}

		return commands;
	}

	//Called when the associated Character is eliminated from the game, clears the target Ball from the list of 'spoken for' Balls, so that the Character's teammates can now attempt to pick up the target Ball
	public void OnCharacterEliminated()
	{
		m_coordinator.ClearSpokenForBall(m_targetBallToPickup);
	}
}
