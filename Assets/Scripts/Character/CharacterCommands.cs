﻿using UnityEngine;

//This script represents the input of either a Keyboard or AI based CommandIssuer to trigger different Character actions
public struct CharacterCommands
{
    public Vector2 m_movement;
    public Vector2 m_aim;
    public bool m_dodge;
	public bool m_pickup;
	public bool m_throw;
	public bool m_catch;
	public bool m_deflect;
}
