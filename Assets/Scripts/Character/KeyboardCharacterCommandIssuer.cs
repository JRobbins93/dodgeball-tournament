﻿using UnityEngine;

//This script queries the input axis that have been set up to respond to key presses and translates those inputs into CharacterCommands
public class KeyboardCharacterCommandIssuer : ICharacterCommandIssuer {

    private int m_playerIndex;

    public KeyboardCharacterCommandIssuer(int playerIndex)
    {
        m_playerIndex = playerIndex;
    }

	public int GetPlayerIndex()
	{
		return m_playerIndex;
	}

    public CharacterCommands GetCommands()
    {
        CharacterCommands result;

		//For each possible action a Character can take, check the associated axis for any input
        float xMovement = GetAxisValue(Constants.AxisNames.HORIZONTAL_MOVEMENT);
        float yMovement = GetAxisValue(Constants.AxisNames.VERTICAL_MOVEMENT);
        result.m_movement = new Vector2(xMovement, yMovement);

        float xAim = GetAxisValue(Constants.AxisNames.HORIZONTAL_AIM);
        float yAim = GetAxisValue(Constants.AxisNames.VERTICAL_AIM);
        result.m_aim = new Vector2(xAim, yAim);

        result.m_dodge = IsAxisNonZero(Constants.AxisNames.DODGE);

		result.m_pickup = IsAxisNonZero(Constants.AxisNames.PICKUP);

		result.m_throw = IsAxisNonZero(Constants.AxisNames.THROW);

		result.m_catch = IsAxisNonZero(Constants.AxisNames.CATCH);

		result.m_deflect = IsAxisNonZero(Constants.AxisNames.DEFLECT);

        return result;
    }

	//Returns the value indicating the input applied along the given axis
    private float GetAxisValue(string axis)
    {
		//Both players have a unique set of axes for their inputs, get the correct axis name based on the input axis and the player number
        string axisFullName = string.Format(Constants.Strings.PLAYER_AXIS_FORMAT, axis, m_playerIndex);
        return Input.GetAxis(axisFullName);
    }

	//Checks whether the given axis has any input applied at all. Used to determine if binary input axes are being used
	private bool IsAxisNonZero(string axis)
	{
        return GetAxisValue(axis) != 0.0f;
	}
}
