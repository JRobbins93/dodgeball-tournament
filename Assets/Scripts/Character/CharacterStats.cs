﻿using System;

//This struct defines various values that determine how a Character behaves
//This class was introduced (rather than using hardcoded constants) to allow for different Character types to be added in the future with unique stats
[Serializable]
public struct CharacterStats
{
    public float m_movementSpeed;
    public float m_dodgeCooldown;
    public float m_dodgeSpeed;
	public float m_throwForce;
}
