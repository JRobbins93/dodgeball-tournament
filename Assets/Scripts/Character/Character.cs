﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Character : MonoBehaviour
{
	private Rigidbody2D m_rigidBodyComponent;
    private SpriteRenderer m_spriteRendererComponent;
	private Animator m_animatorComponent;

	//The source of input commands for the Character, depending on the 
	private ICharacterCommandIssuer m_commandIssuer;

    private EventManager m_eventManager;

	//The index of the team that the Character is on
	private int m_teamIndex;

	//Statistics that govern the way different actions are performed by the Character
	[SerializeField]
	private CharacterStats m_stats = default(CharacterStats);

	//References to objects that make up the indicator shown above human-controlled Characters
	[SerializeField]
	private GameObject m_playerIndicatorCanvas = null;
    [SerializeField]
    private Image m_playerIndicatorImage = null;
    [SerializeField]
    private Text m_playerIndicatorText = null;

	//Cooldown timers
    private float m_timeSinceLastDodge;
    private float m_timeSinceLastPickup;
	private float m_timeSinceLastThrow;
	private float m_timeSinceLastCatch;
	private float m_timeSinceLastDeflect;

	//Variables set whilst evaluating input for actions that are performed during physics steps
	private Vector2 m_movementDirectionNextPhysicsStep;
	private bool m_performDodgeNextPhysicsStep;
	private bool m_performThrowNextPhysicsStep;

	//The vector that a held Ball will be thrown in if the corresponding command is issued
    private Vector2 m_aimDirection;

	//The Ball the Character is currently holding and can perform various actions with
	private Ball m_heldBall;

	//List of Balls within each of the Character's trigger zones on which different actions can potentially be performed
	private List<Ball> m_ballsWithinPickupTriggerZone;
	private List<Ball> m_ballsWithinCatchTriggerZone;
	private List<Ball> m_ballsWithinDeflectTriggerZone;

    private Game m_game;

	public void Init(ICharacterCommandIssuer commandIssuer, int teamIndex, Game game)
	{
		m_commandIssuer = commandIssuer;
		m_teamIndex = teamIndex;
        m_game = game;

		//Sprites for Character's on both teams were made facing the same direction,
		//If the Character is on the second team, flip their sprite
        m_spriteRendererComponent.flipX = (teamIndex != 0);

		//If the Character is recieving commands from a keyboard, then it must be controlled by a human player
		//We therefore need to set up the indicator UI above the Character to display which numbered player is controlling them
        KeyboardCharacterCommandIssuer keyboardCommandIssuer = m_commandIssuer as KeyboardCharacterCommandIssuer;
		if(keyboardCommandIssuer != null)
		{
			m_playerIndicatorCanvas.SetActive(true);
            Color teamColour = Constants.Colours.TEAM_COLOURS[teamIndex];
            m_playerIndicatorImage.color = teamColour;
            m_playerIndicatorText.color = teamColour;
			
			//Offset text by 1 so we don't display "P0"
            m_playerIndicatorText.text = String.Format(Constants.Strings.PLAYER_INDICATOR_TEXT_FORMAT, keyboardCommandIssuer.GetPlayerIndex() + 1);
		}
		else
		{
			//Character is controlled by AI, no need to display player indicator
            m_playerIndicatorCanvas.SetActive(false);
		}
	}

	private void Awake()
	{
		m_eventManager = GameObject.FindObjectOfType<EventManager>();

		m_rigidBodyComponent = GetComponent<Rigidbody2D>();
		m_spriteRendererComponent = GetComponent<SpriteRenderer>();
		m_animatorComponent = GetComponent<Animator>();

		m_ballsWithinPickupTriggerZone = new List<Ball>();
		m_ballsWithinCatchTriggerZone = new List<Ball>();
		m_ballsWithinDeflectTriggerZone = new List<Ball>();

		//Set all cooldowns to be at their limit so the Character can perform all actions at the start of the game
        m_timeSinceLastDodge = m_stats.m_dodgeCooldown;
		m_timeSinceLastPickup = Constants.GameRules.ACTION_COOLDOWN;
		m_timeSinceLastThrow = Constants.GameRules.ACTION_COOLDOWN;
		m_timeSinceLastCatch = Constants.GameRules.ACTION_COOLDOWN;
		m_timeSinceLastDeflect = Constants.GameRules.ACTION_COOLDOWN;
	}

	private void Update()
	{
		//If the game is still counting down to begin, don't want Characters to do anything
		if(m_game != null && !m_game.HasStarted())
		{
			return;
		}

		//Increment all cooldowns
        m_timeSinceLastDodge += Time.deltaTime;
        m_timeSinceLastPickup += Time.deltaTime;
		m_timeSinceLastThrow += Time.deltaTime;
		m_timeSinceLastCatch += Time.deltaTime;
		m_timeSinceLastDeflect += Time.deltaTime;

		//If the Character has been holding the Ball for too long, surreneder it to the other team
		if (m_heldBall != null && m_timeSinceLastPickup >= Constants.GameRules.TIME_UNTIL_DEAD_BALL)
		{
			PerformGiveUpDeadBall();
		}

        if (m_commandIssuer != null)
        {
            CharacterCommands commands = m_commandIssuer.GetCommands();

			//Carry out actions if the appropriate input was given and the Character is capable of performing them
			if (commands.m_pickup && CanPerformPickup())
            {
                PerformPickup();
            }

            if (commands.m_catch && CanPerformCatch())
            {
                PerformCatch();
            }

            if (commands.m_deflect && CanPerformDeflect())
            {
                PerformDeflect();
            }

            m_performDodgeNextPhysicsStep = (commands.m_dodge && CanPerformDodge());
            m_performThrowNextPhysicsStep = (commands.m_throw && CanPerformThrow());

			m_movementDirectionNextPhysicsStep = commands.m_movement;
			m_aimDirection = commands.m_aim;

			//Trigger run animation if the Character is moving, otherwise rever to the idle animation
			m_animatorComponent.SetBool(Constants.CharacterAnimationParameters.IS_RUNNING, m_movementDirectionNextPhysicsStep != Vector2.zero);
        }
	}

	private void FixedUpdate()
	{
		//Move in the direction set last time the input was queried, scaling the magnitude alongside the Characters movement speed stat
		m_rigidBodyComponent.velocity = m_movementDirectionNextPhysicsStep.normalized * m_stats.m_movementSpeed;

		//Perform physics based actions if the associated command has been given
		if (m_performDodgeNextPhysicsStep)
		{
			PerformDodge();
		}

		if(m_performThrowNextPhysicsStep)
		{
			PerformThrow(m_aimDirection);
		}

		//Ensure the Character is within their side of the court
		Vector2 positionToApply = m_rigidBodyComponent.position;
		positionToApply.x = Mathf.Clamp(positionToApply.x, Constants.EntityDimensions.CHARACTER_MIN_X[m_teamIndex], Constants.EntityDimensions.CHARACTER_MAX_X[m_teamIndex]);
		positionToApply.y = Mathf.Clamp(positionToApply.y, Constants.EntityDimensions.CHARACTER_MIN_Y, Constants.EntityDimensions.CHARACTER_MAX_Y);
		m_rigidBodyComponent.position = positionToApply;

		//Reset the values to govern physics behaviour ahead of the next step
		m_movementDirectionNextPhysicsStep = Vector2.zero;
		m_performDodgeNextPhysicsStep = false;
		m_performThrowNextPhysicsStep = false;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		string layerName = LayerMask.LayerToName(collision.gameObject.layer);
		if (layerName == Constants.LayerNames.BACK_WALL)
		{
			OnBackWallCollisionEnter();
		}
	}

	//Checks whether the Character can perform the dodge action
	private bool CanPerformDodge()
	{
		//Dodging makes no sense if the Character is still, since the dodge direction is based on movement direction
		return m_timeSinceLastDodge >= m_stats.m_dodgeCooldown && m_rigidBodyComponent.velocity != Vector2.zero;
	}

	//Checks whether the Character can perform the throw action
	private bool CanPerformThrow()
	{
		//Held Balls can't be thrown until they have been put into play
		//Need to check time since last pickup since otherwise the throw would occur on the same frame as the pickup
		return m_heldBall != null && m_heldBall.IsInPlay() && m_timeSinceLastThrow >= Constants.GameRules.ACTION_COOLDOWN && m_timeSinceLastPickup >= Constants.GameRules.ACTION_COOLDOWN;
	}

	//Checks whether the Character can perform the pickup action
	private bool CanPerformPickup()
	{
		//Need to check time since last throw since otherwise if the Character throws a Ball while standing on top of another one, they could pick it up in the very next frame
		return m_heldBall == null && m_ballsWithinPickupTriggerZone.Count > 0 && m_timeSinceLastPickup >= Constants.GameRules.ACTION_COOLDOWN && m_timeSinceLastThrow >= Constants.GameRules.ACTION_COOLDOWN;
	}

	//Checks whether the Character can perform the pickup action
	private bool CanPickupBall(Ball ball)
    {
		//Balls that are not 'in play' may only be picked up by Characters from one particular team
		return (ball.IsOnFloor() && (ball.IsInPlay() || ball.GetInitialPickupTeamIndex() == m_teamIndex));
    }

	//Checks whether the Character can perform the catch action
	private bool CanPerformCatch()
	{
		return m_heldBall == null && m_ballsWithinCatchTriggerZone.Count > 0 && m_timeSinceLastCatch >= Constants.GameRules.ACTION_COOLDOWN;
	}

	//Checks whether the Character can perform the deflect action
	private bool CanPerformDeflect()
	{
		return m_heldBall != null && m_ballsWithinDeflectTriggerZone.Count > 0 && m_timeSinceLastDeflect >= Constants.GameRules.ACTION_COOLDOWN;
	}

	//Checks if a Ball was thrown by a Character on the opposing team
    private bool WasBallThrownByOpponent(Ball ball)
    {
		if(ball.IsOnFloor())
		{
			return false;
		}

		Character lastThrowingCharacter = ball.GetLastThrowingCharacter();

		return (lastThrowingCharacter != null && lastThrowingCharacter.GetTeamIndex() != GetTeamIndex());
    }

    public int GetTeamIndex()
    {
        return m_teamIndex;
    }

	public bool IsInPlay()
	{
		return enabled;
	}

	public Ball GetHeldBall()
	{
		return m_heldBall;
	}

	//Returns the Ball from the given list that is nearest to the Character for which the passed boolean function returns true (if any)
	private Ball GetClosestBallInList(List<Ball> balls, Func<Ball, bool> conditionFunction)
	{
		Ball closestBall = null;
		float squareDistanceToClosestBall = float.MaxValue;

		//Iterate through the list of Balls
		for (int ballIndex = 0; ballIndex < balls.Count; ballIndex++)
		{
			Ball currentBall = balls[ballIndex];

			//Check the boolean condition for the current Ball
			if(!conditionFunction(currentBall))
			{
				//Boolean check failed, move onto the next Ball
				continue;
			}

			//Compare the distance to the current Ball with the stored closest one
			Vector3 directionToCurrentBall = currentBall.transform.position - transform.position;
			float squareDistanceToCurrentBall = directionToCurrentBall.sqrMagnitude;

			//Current Ball is closer, replace stored closest Ball with the current one
			if (squareDistanceToCurrentBall < squareDistanceToClosestBall)
			{
				closestBall = currentBall;
				squareDistanceToClosestBall = squareDistanceToCurrentBall;
			}
		}

		return closestBall;
	}

	//Carry out the dodge action, making the Character move rapidly in their current movement direction
	private void PerformDodge()
	{
		//Continue to move in the current direction, but much quicker
		m_rigidBodyComponent.velocity *= m_stats.m_dodgeSpeed;
		m_timeSinceLastDodge = 0.0f;

		//Send the event as some TutorialStates need to be made aware
		m_eventManager.SendEvent(Constants.EventNames.ON_CHARACTER_DODGED);
	}

	//Carry out the throw action, launching the held ball in the provided direction
	private void PerformThrow(Vector2 throwDirection)
	{
		if(throwDirection == Vector2.zero)
		{
			return;
		}

		//Throw in the direction set last time the input was queried, scaling the magnitude alongside the Characters throw force stat
		m_heldBall.OnThrown(throwDirection.normalized * m_stats.m_throwForce, this);
		m_heldBall = null;

		//Play throw animation
		m_animatorComponent.SetTrigger(Constants.CharacterAnimationParameters.ON_THROW);

		m_timeSinceLastThrow = 0.0f;
	}

	//Carry out the pick up action
	private void PerformPickup()
	{
		Ball ballToPickup = GetClosestBallInList(m_ballsWithinPickupTriggerZone, CanPickupBall);

		if (ballToPickup == null)
		{
			//There are Balls close enough to pick up, but we can't perform the action on them at this time
			return;
		}

		m_heldBall = ballToPickup;
		m_heldBall.transform.SetParent(transform);
		m_heldBall.OnPickedUp();

		m_timeSinceLastPickup = 0.0f;
    }

	//Carry out the catch action, eliminating the Character who threw the caught Ball
	private void PerformCatch()
	{
		Ball ballToCatch = GetClosestBallInList(m_ballsWithinCatchTriggerZone, WasBallThrownByOpponent);

		if(ballToCatch == null)
		{
			//There are Balls close enough to catch, but we can't perform the action on them at this time
			return;
		}

		m_heldBall = ballToCatch;
		m_heldBall.transform.SetParent(transform);
		m_heldBall.OnCaught();

        if (m_game != null)
        {
			//When a Character catches a Ball, a previously eliminated teammate of theirs is returned to the game
            m_game.ReviveCharacter(m_teamIndex);
        }

        m_timeSinceLastPickup = 0.0f;
        m_timeSinceLastCatch = 0.0f;
		m_timeSinceLastDeflect = 0.0f;
	}

	//Carry out the deflect action, sending the deflected ball away from the Character
	private void PerformDeflect()
	{
		Ball ballToDeflect = GetClosestBallInList(m_ballsWithinDeflectTriggerZone, WasBallThrownByOpponent);

		if(ballToDeflect == null)
		{
			//There are Balls close enough to deflect, but we can't perform the action on them at this time
			return;
		}

		ballToDeflect.OnDeflected();

        m_timeSinceLastDeflect = 0.0f;
    }

	//Called when the Character has been holding a Ball for too long and it must be surrendered to the other team
	private void PerformGiveUpDeadBall()
	{
		m_heldBall.OnDeadBall();
		m_heldBall = null;
	}

	//Called when a Ball moves into one of the Character's trigger zones
	public void OnBallEnteredTriggerZone(Ball ball, CharacterTriggerZone.eCharacterTriggerZoneType triggerZoneType)
	{
		//Remove the Ball from the list of Balls close enough to perform the respective action on
		//Unless the Ball is close enough to eliminate the Character
		switch (triggerZoneType)
		{
			case CharacterTriggerZone.eCharacterTriggerZoneType.Pickup:
				m_ballsWithinPickupTriggerZone.Add(ball);
				break;
			case CharacterTriggerZone.eCharacterTriggerZoneType.Catch:
				m_ballsWithinCatchTriggerZone.Add(ball);
				break;
			case CharacterTriggerZone.eCharacterTriggerZoneType.Deflect:
				m_ballsWithinDeflectTriggerZone.Add(ball);
				break;
			case CharacterTriggerZone.eCharacterTriggerZoneType.Elimination:
				OnBallEnteredEliminationTriggerZone(ball);
				break;
		}
	}

	//Called when a Ball leaves one of the Character's trigger zones
	public void OnBallExitedTriggerZone(Ball ball, CharacterTriggerZone.eCharacterTriggerZoneType triggerZoneType)
	{
		//Remove the Ball from the list of Balls close enough to perform the respective action on
		switch (triggerZoneType)
		{
			case CharacterTriggerZone.eCharacterTriggerZoneType.Pickup:
				m_ballsWithinPickupTriggerZone.Remove(ball);
				break;
			case CharacterTriggerZone.eCharacterTriggerZoneType.Catch:
				m_ballsWithinCatchTriggerZone.Remove(ball);
				break;
			case CharacterTriggerZone.eCharacterTriggerZoneType.Deflect:
				m_ballsWithinDeflectTriggerZone.Remove(ball);
				break;
		}
	}

	//Called when a Ball enters the elimination trigger zone
	private void OnBallEnteredEliminationTriggerZone(Ball ball)
	{
		//If the Character has not already been eliminated and the Ball was thrown by an opponent, eliminate them
		if (enabled && WasBallThrownByOpponent(ball))
        {
            OnEliminated();
            ball.OnEliminatedCharacter();
        }
	}

	//Called when the Character comes into contact with the back walls of the court, meaning any Balls they are carrying are put into play
	private void OnBackWallCollisionEnter()
	{
		if(m_heldBall != null)
		{
			m_heldBall.SetAsInPlay();
		}
	}

	//Called when a Ball the Character threw was caught by an opponent, eliminating them from the game
    public void OnThrownBallCaught()
    {
        OnEliminated();
    }

	//Called if the Character is still alive and their team wins the game
	public void OnVictory()
	{
		//Play victory animation and stop Character from moving
		m_animatorComponent.SetTrigger(Constants.CharacterAnimationParameters.ON_VICTORY);
		m_rigidBodyComponent.velocity = Vector2.zero;
		enabled = false;
	}

	//Called when the Character is eliminated from the game
    private void OnEliminated()
    {
		//Drop any Ball the Character is holding so other Character can pick it up
		if (m_heldBall != null)
		{
			m_heldBall.OnDropped();
			m_heldBall = null;
		}

		//Stop the Update function of this script from being called every frame
		enabled = false;

		//If the Character is controlled by an AI command issuer, then it must be informed that the Character has been eliminated
		AICharacterCommandIssuer aiCommandIssuer = m_commandIssuer as AICharacterCommandIssuer;
		if(aiCommandIssuer != null)
		{
			aiCommandIssuer.OnCharacterEliminated();
		}

		m_eventManager.SendEvent(Constants.EventNames.ON_CHARACTER_ELIMINATED);

		//Trigger the elimination animation
		m_animatorComponent.SetTrigger(Constants.CharacterAnimationParameters.ON_ELIMINATED);

		//Want eliminated Characters to be drawn behind ones who are still playing
		m_spriteRendererComponent.sortingOrder = 0;

		if (m_game != null)
        {
			//Inform the game that the Character has been eliminated
			//So that it can be added to the queue of Characters to be revived upon one their teammates catching an opponent's thrown Ball
            m_game.OnCharacterEliminated(this);
        }

		//Since velocity was previously being set in the Update function, zero it so the eliminated Character comes to a stop
		m_rigidBodyComponent.velocity = Vector2.zero;

		//Disable player indicator UI
		m_playerIndicatorCanvas.SetActive(false);
	}

	//Called when the Character is returned to the game upon one of their teammates catching a Ball thrown by an opponent
	public void OnRevived()
    {
		//Characters should return to the game at the back wall of their team's side of the court
		Vector2 position = transform.position;
		position.x = (GetTeamIndex() == 0) ? Constants.EntityDimensions.CHARACTER_MIN_X[0] : Constants.EntityDimensions.CHARACTER_MAX_X[1];
		transform.position = position;

		//Reactive the script so that Update gets called once again
		enabled = true;

		//Revert the Character sprite that was changed upon elimination
		m_animatorComponent.SetTrigger(Constants.CharacterAnimationParameters.ON_REVIVED);

		//Want non-eliminated Characters to be drawn in front of eliminated ones
		m_spriteRendererComponent.sortingOrder = 1;

		//Reactive player indicator UI that would have turned off upon elimination
		m_playerIndicatorCanvas.SetActive(m_commandIssuer is KeyboardCharacterCommandIssuer);
	}
}
