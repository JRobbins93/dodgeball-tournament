﻿//Interface that must be implemented in order for a class to dictate which actions a Character performs.
//The associated Character will call the GetCommands method each frame to ask for input
//Introduced to abstract away the source of the commands
//Implemented by KeyboardCharacterCommandIssuer and AICharacterCommandIssuer
//In the future, could be implemented by a class that queried a gamepad for input rather than a keyboard
public interface ICharacterCommandIssuer {
    
    CharacterCommands GetCommands();
}
