﻿using UnityEngine;

//This script represents the invisible zones that surround a player and check whether a Ball is close enough to the Character for them to perform different actions
public class CharacterTriggerZone : MonoBehaviour {

	//If a Ball is within the Pickup, Catch or Deflect TriggerZones, then the Ball is close enough for the corresponding action to be performed on it
	//If a Ball is within the Elimination TriggerZone, then the associated Character will be eliminated from the game
	public enum eCharacterTriggerZoneType { Pickup, Catch, Deflect, Elimination }

	[SerializeField]
	private Character m_character = null;

	[SerializeField]
	private eCharacterTriggerZoneType m_triggerZoneType = default(eCharacterTriggerZoneType);

	private void OnTriggerEnter2D(Collider2D other)
	{
		Ball ball = other.gameObject.GetComponent<Ball>();

		//If the object that entered the TriggerZone isn't a ball, do nothing, otherwise get the Character to respond based on the TriggerZone's type
		if(ball == null)
		{
			return;
		}
        
		m_character.OnBallEnteredTriggerZone(ball, m_triggerZoneType);
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		Ball ball = other.gameObject.GetComponent<Ball>();

		//If the object that exited the TriggerZone isn't a ball, do nothing, otherwise get the Character to respond based on the TriggerZone's type
		if (ball == null)
		{
			return;
		}

		m_character.OnBallExitedTriggerZone(ball, m_triggerZoneType);
	}

	public eCharacterTriggerZoneType GetTriggerZoneType()
	{
		return m_triggerZoneType;
	}
}
