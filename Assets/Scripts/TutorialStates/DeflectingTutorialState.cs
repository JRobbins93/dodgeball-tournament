﻿using System.Collections.Generic;
using UnityEngine;

//This script defines the seventh stage of the tutorial in which the player must deflect a ball thrown by an AI opponent
//The player can fail this TutorialState by:
//	Getting hit by the thrown ball
//	Failing to deflect the ball before it drops to the ground
//	Eliminating their opponent, either by catching the ball they throw or throwing their own ball at them
public class DeflectingTutorialState : TutorialState
{
    private Character m_playerCharacter;
	private Character m_opponentCharacter;
	private Ball m_playerBall;
	private Ball m_opponentBall;

    public DeflectingTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		//Since no frames will have passed between exiting the last TutorialState and entering this one, the opposing AI Character will not yet have been deleted.
		m_playerCharacter = FindPlayerCharacter();
		m_playerBall = GameObject.FindObjectOfType<Ball>();

		base.OnEnter();

		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_DEFLECTED, OnBallDeflected);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_HIT_BACK_WALL, OnMissedThrow);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_LOST_VELOCITY, OnMissedThrow);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_DEAD_BALL, OnDeadBall);
    }

	public override void OnExit()
	{
		base.OnExit();

		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_DEFLECTED, OnBallDeflected);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_HIT_BACK_WALL, OnMissedThrow);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_LOST_VELOCITY, OnMissedThrow);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_DEAD_BALL, OnDeadBall);
	}

    public override string GetTutorialText()
    {
        return Constants.Strings.DEFLECTING_TUTORIAL_INSTRUCTION;
    }

    private void OnBallDeflected()
    {
		//Once the action has been performed, we can move onto the next TutorialState
		StartExitCounter();
    }

	private void OnCharacterEliminated()
	{
		//If either character has been eliminated, we need to reset the TutorialState.
		//Look at whether the player's Character or the AI character are still in play to see which message to display to the user
		if (m_opponentCharacter.IsInPlay())
		{
			StartResetCounter(Constants.Strings.DEFLECTING_TUTORIAL_ON_PLAYER_ELIMINATED_INSTRUCTION);
		}
		else
		{
			StartResetCounter(Constants.Strings.DEFECTING_TUTORIAL_ON_OPPONENT_ELIMINATED_INSTRUCTION);
		}
	}

	private void OnMissedThrow()
	{
		StartResetCounter(Constants.Strings.DEFLECTING_TUTORIAL_ON_MISSED_THROW_INSTRUCTION);
	}

	private void OnDeadBall()
	{
		StartResetCounter(Constants.Strings.DEFLECTING_TUTORIAL_ON_DEAD_BALL_INSTRUCTIONS);
	}

	public override void Reset()
	{
		//If we're resetting this TutorialState then the player Character could have potentially been eliminated, we'll need to spawn a new one
		GameObject.Destroy(m_playerCharacter.gameObject);
		m_playerCharacter = SpawnPlayerCharacter();

		//Will need to create a new Ball upon reset since the player will no longer be holding one.
		GameObject.Destroy(m_playerBall.gameObject);
		m_playerBall = SpawnBall(0, Constants.EntityDimensions.COURT_CENTRE_Y - Constants.EntityDimensions.COURT_QUATER_HEIGHT);

		base.Reset();
	}

	protected override void SpawnObjects()
	{
		//Create a Ball to be picked up by the opposing Character
		m_opponentBall = SpawnBall(1, Constants.EntityDimensions.COURT_CENTRE_Y + Constants.EntityDimensions.COURT_QUATER_HEIGHT);

		//Create an AI opponent who will throw the Ball at the player
		AICoordinator coordinator = new AICoordinator();

		m_opponentCharacter = SpawnOpponentCharacter(coordinator);

		coordinator.SetCharacters(new List<Character> { m_playerCharacter, m_opponentCharacter });

		coordinator.SetBalls(new List<Ball> { m_playerBall, m_opponentBall });
	}

	protected override void DestroyObjects()
	{
		GameObject.Destroy(m_opponentBall.gameObject);
		GameObject.Destroy(m_opponentCharacter.gameObject);
	}
}
