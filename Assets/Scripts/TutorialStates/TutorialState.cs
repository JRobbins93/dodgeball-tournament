﻿using UnityEngine;

//This script is an abstract class defining all shared behaviour for the different stages of the tutorial
public abstract class TutorialState {

	//Reference to the Tutorial scene script
    private Tutorial m_tutorial;

	//Counter used to time transition to the next state, used to avoid a jarring transition
	private float m_exitCounter;
	private bool m_isExitCounterTicking;

	//Counter used to reset the current state if the user makes a mistake, used to avoid a jarring transition
	private float m_resetCounter;
	private bool m_isResetCounterTicking;

	//The message to be displayed after the current state resets following a mistake made by the user
	private string m_resetMessage;

	public TutorialState(Tutorial tutorial)
    {
        m_tutorial = tutorial;
    }

	//Begins the counter whose termination will cause the Tutorial to move to the next TutorialState
	public void StartExitCounter()
	{
		m_exitCounter = Constants.Tutorial.STATE_EXIT_TIMER;
		m_isExitCounterTicking = true;
	}

	//Begins the counter whose termination will cause the current TutorialState to be reset
	//Called following user error, the provided message will be displayed upon reset
	public void StartResetCounter(string message)
	{
		m_resetCounter = Constants.Tutorial.STATE_RESET_TIMER;
		m_isResetCounterTicking = true;
		m_resetMessage = message;
	}

	//Decreases the timer that counts down to the Tutorial moving to the next TutorialState
	public void DecreaseExitCounter(float amount)
	{
		m_exitCounter -= amount;
	}

	//Decreases the timer that counts down to the current TutorialState being reset
	public void DecreaseResetCounter(float amount)
	{
		m_resetCounter -= amount;
	}

	public float GetExitCounter()
	{
		return m_exitCounter;
	}

	public float GetResetCounter()
	{
		return m_resetCounter;
	}

	public bool IsExitCounterTicking()
	{
		return m_isExitCounterTicking;
	}

	public bool IsResetCounterTicking()
	{
		return m_isResetCounterTicking;
	}

    protected Tutorial GetTutorial()
    {
        return m_tutorial;
    }

	//Shows the provided message within a popup window
    protected void ShowTutorialMessage(string message)
    {
        m_tutorial.ShowTutorialMessage(message);
    }

	//Creates a Character that is controlled by a human player
    protected Character SpawnPlayerCharacter()
    {
		//Spawn the player in the centre of the left hand side of the court
        Vector2 position = new Vector2(Constants.EntityDimensions.COURT_MIN_X + Constants.EntityDimensions.COURT_QUATER_WIDTH, Constants.EntityDimensions.COURT_CENTRE_Y);

        GameObject characterObject = GameObject.Instantiate(GetTutorial().GetPlayerCharacterPrefab());
        characterObject.transform.position = position;

        Character character = characterObject.GetComponent<Character>();
        character.Init(new KeyboardCharacterCommandIssuer(0), 0, null);

        return character;
    }

	//Spawns a Character that is on the opposing side to the player, controlled by AI
    protected Character SpawnOpponentCharacter(AICoordinator coordinator)
    {
		//Spawn the player in the centre of the right hand side of the court
		Vector2 position = new Vector2(Constants.EntityDimensions.COURT_CENTRE_X + Constants.EntityDimensions.COURT_QUATER_WIDTH, Constants.EntityDimensions.COURT_CENTRE_Y);

        GameObject characterObject = GameObject.Instantiate(GetTutorial().GetOppoentPlayerPrefab());
        characterObject.transform.position = position;

        Character character = characterObject.GetComponent<Character>();

		AICharacterCommandIssuer commandIssuer = null;

		//If a AICoordinator has been provided, create an AI agent of that coordinator, otherwise leave the agent as null to produce a dummy opponent
		if(coordinator != null)
		{
			commandIssuer = new AICharacterCommandIssuer(character, coordinator);
		}
		
        character.Init(commandIssuer, 1, null);

        return character;
	}

	//Spawns a Character that is on the opposing side to the player, this Character will not perform any action
	protected Character SpawnDummyOpponentCharacter()
    {
		return SpawnOpponentCharacter(null);
    }

	//Spawns a ball at the provided position that can only be picked up by members of the team with the matching index
	protected Ball SpawnBall(int initialPickupTeamIndex, float ballYPosition)
    {
		//Spawn balls along the centre line of the court
        Vector2 position = new Vector2(Constants.EntityDimensions.COURT_CENTRE_X, ballYPosition);

        GameObject ballObject = GameObject.Instantiate(GetTutorial().GetBallPrefab());
        ballObject.transform.position = position;

        Ball ball = ballObject.GetComponent<Ball>();
        ball.Init(initialPickupTeamIndex);

        return ball;
    }

	//Returns the Character within the Tutorial scene that is controlled by the player
	protected Character FindPlayerCharacter()
	{
		Character[] characters = GameObject.FindObjectsOfType<Character>();

		//We iterate through all of the Characters in the scene and check the team index to determine which is the player's
		for (int characterIndex = 0; characterIndex < characters.Length; characterIndex++)
		{
			Character currentCharacter = characters[characterIndex];
			if (currentCharacter.GetTeamIndex() == 0)
			{
				return currentCharacter;
			}
		}

		return null;
	}

	//Called when the TutorialState becomes the active one for the Tutorial scene. Any event callbacks should be regisitered within here.
	public virtual void OnEnter()
	{
		SpawnObjects();

		ShowTutorialMessage(GetTutorialText());
	}

	//Called when the TutorialState is no longer the active one for the Tutorial scene. Any event callbacks should be deregisitered within here.
	public virtual void OnExit()
	{
		DestroyObjects();
	}

	//Called when the TutorialState needs to be reset as a result of the user making some mistake
	public virtual void Reset()
	{
		DestroyObjects();

		SpawnObjects();

		//Show the message that was stored when the reset counter was started
		ShowTutorialMessage(m_resetMessage);

		m_isResetCounterTicking = false;
	}

	//Spawn the objects that are used within the particular TutorialState that will not exist upon entering into the TutorialState
	protected abstract void SpawnObjects();

	//Destroys the objects that are not used within the subsequant TutorialState
	protected abstract void DestroyObjects();

	//Gets the message that should be displayed upon entering into the TutorialState or when the 'Help' button is pressed from within the Tutorial scene
	public abstract string GetTutorialText();
}
