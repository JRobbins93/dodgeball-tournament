﻿using UnityEngine;

//This script defines the first stage of the tutorial in which the player must move to a series of markers on the ground to teach them about running controls
public class MovingTutorialState : TutorialState
{
	//References to the triggers that the player must move to in order to pass this stage of the tutorial
	private GameObject[] m_movementTriggerObjects;

    public MovingTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		base.OnEnter();

		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_MOVEMENT_TRIGGER_ACTIVATED, OnMovementTriggerActivated);
	}

	public override void OnExit()
	{
		base.OnExit();

		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_MOVEMENT_TRIGGER_ACTIVATED, OnMovementTriggerActivated);
	}

    public override string GetTutorialText()
    {
        return Constants.Strings.MOVING_TUTORIAL_INSTRUCTION;
    }

	//Event callback triggered when the player moves onto a movement trigger
	private void OnMovementTriggerActivated()
	{
		//Iterate through the movement triggers, if none of them are active, then the player has collided with each of them and has therefore passed this TutorialState
		for(int movementTriggerIndex = 0; movementTriggerIndex < m_movementTriggerObjects.Length; movementTriggerIndex++)
		{
			GameObject currentMovementTriggerObject = m_movementTriggerObjects[movementTriggerIndex];

			if(currentMovementTriggerObject.activeSelf)
			{
				return;
			}
		}

		StartExitCounter();
	}

	protected override void SpawnObjects()
	{
		//Since this is the first TutorialState, need to spawn player's Character
		Character character = SpawnPlayerCharacter();

		//Place MovementTriggers in a circle, centered at the player's starting position, so that they must move in all directions to pass the TutorialState
		Vector2 centre = character.transform.position;

		m_movementTriggerObjects = new GameObject[Constants.Tutorial.NUMBER_OF_MOVEMENT_TRIGGERS_TO_sPAWN];

		for (int movementTriggerObjectIndex = 0; movementTriggerObjectIndex < m_movementTriggerObjects.Length; movementTriggerObjectIndex++)
		{
			float theta = Mathf.PI * 2.0f * ((float)movementTriggerObjectIndex / m_movementTriggerObjects.Length);

			float x = centre.x + Constants.Tutorial.MOVEMENT_TRIGGER_LAYOUT_RADIUS * Mathf.Cos(theta);
			float y = centre.y + Constants.Tutorial.MOVEMENT_TRIGGER_LAYOUT_RADIUS * Mathf.Sin(theta);

			GameObject movementTriggerObject = GameObject.Instantiate(GetTutorial().GetMovementTriggerPrefab());

			movementTriggerObject.transform.position = new Vector2(x, y);

			//Keep reference to MovementTrigger object so we can check later if it's been collided with
			m_movementTriggerObjects[movementTriggerObjectIndex] = movementTriggerObject;
		}
	}

	protected override void DestroyObjects()
	{
		//Get rid of the MovementTriggers since they are only relevant for this TutorialState
		for (int movementTriggerIndex = 0; movementTriggerIndex < m_movementTriggerObjects.Length; movementTriggerIndex++)
		{
			GameObject.Destroy(m_movementTriggerObjects[movementTriggerIndex]);
		}
	}
}
