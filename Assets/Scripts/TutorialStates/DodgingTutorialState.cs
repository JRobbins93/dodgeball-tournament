﻿//This script defines the second stage of the tutorial in which the player must perform a dodge
public class DodgingTutorialState : TutorialState
{
    public DodgingTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		base.OnEnter();

        GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_CHARACTER_DODGED, OnCharacterDodged);
	}

	public override void OnExit()
	{
		base.OnExit();

        GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_CHARACTER_DODGED, OnCharacterDodged);
	}

    public override string GetTutorialText()
    {
        return Constants.Strings.DODGING_TUTORIAL_INSTRUCTION;
    }

    private void OnCharacterDodged()
    {
		//Once the action has been performed, we can move onto the next TutorialState
		StartExitCounter();
    }

	protected override void SpawnObjects()
	{
	}

	protected override void DestroyObjects()
	{
	}
}
