﻿using UnityEngine;

//This script defines the fourth stage of the tutorial in which the player must put a ball into play by carrying it to the back wall
//The player can fail this TutorialState by:
//	Holding a ball for too long and having to give it up as a 'dead ball'
public class PuttingBallIntoPlayTutorialState : TutorialState
{
    private Ball m_ball;

    public PuttingBallIntoPlayTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		//Upon entering the scene, the player will be carrying a ball, get a reference to that rather than spawning, and making them pick up, a whole new one
		m_ball = GameObject.FindObjectOfType<Ball>();

		base.OnEnter();

        GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_PUT_INTO_PLAY, OnBallPutIntoPlay);
        GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_DEAD_BALL, OnDeadBall);
	}

	public override void OnExit()
	{
		base.OnExit();

        GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_PUT_INTO_PLAY, OnBallPutIntoPlay);
        GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_DEAD_BALL, OnDeadBall);
    }

    public override string GetTutorialText()
    {
        return Constants.Strings.PUTTING_BALL_INTO_PLAY_INSTRUCTION;
    }

    private void OnBallPutIntoPlay()
    {
		//Once the action has been performed, we can move onto the next TutorialState
		StartExitCounter();
    }

    private void OnDeadBall()
    {
		StartResetCounter(Constants.Strings.PUTTING_BALL_INTO_PLAY_TUTORIAL_ON_DEAD_BALL_INSTRUCTION);
    }

	public override void Reset()
	{
		base.Reset();

		//Will need to create a new Ball upon reset since the player will no longer be holding one.
		GameObject.Destroy(m_ball.gameObject);

		m_ball = SpawnBall(0, Constants.EntityDimensions.COURT_CENTRE_Y);
	}

	protected override void SpawnObjects()
	{
	}

	protected override void DestroyObjects()
	{
		//We don't need to destroy the Ball upon exit since the subsequent TutorialState requires the player to hold a Ball
	}
}
