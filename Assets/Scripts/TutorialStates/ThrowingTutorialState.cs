﻿using UnityEngine;

//This script defines the fifth stage of the tutorial in which the player must throw a ball to eliminate a dummy opponent
//The player can fail this TutorialState by:
//	Holding a ball for too long and having to give it up as a 'dead ball'
//	Failing to hit the opponent with their thrown ball
public class ThrowingTutorialState : TutorialState
{
	private Character m_dummyCharacter;
    private Ball m_ball;

    public ThrowingTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		//Upon entering the scene, the player will be carrying a ball, get a reference to that rather than spawning, and making them pick up, a whole new one
		m_ball = GameObject.FindObjectOfType<Ball>();

		base.OnEnter();

		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_DEAD_BALL, OnDeadBall);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_HIT_BACK_WALL, OnMissedThrow);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_LOST_VELOCITY, OnMissedThrow);
	}

	public override void OnExit()
	{
		base.OnExit();

		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_DEAD_BALL, OnDeadBall);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_HIT_BACK_WALL, OnMissedThrow);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_LOST_VELOCITY, OnMissedThrow);
	}

    public override string GetTutorialText()
    {
        return Constants.Strings.THROWING_TUTORIAL_INSTRUCTION;
    }

    private void OnCharacterEliminated()
    {
		//The player has eliminated the dummy, we can move onto the next TutorialState
		StartExitCounter();
    }

	private void OnDeadBall()
	{
		StartResetCounter(Constants.Strings.THROWING_TUTORIAL_ON_DEAD_BALL_INSTRUCTION);
	}

	private void OnMissedThrow()
	{
		//If the ball hits a back wall or loses its velocity, then the player must have missed, need to reset the TutorialState
		StartResetCounter(Constants.Strings.THROWING_TUTORIAL_ON_MISSED_THROW_INSTRUCTION);
	}

	public override void Reset()
	{
		base.Reset();

		//Will need to create a new ball upon reset since the player will no longer be holding one
		m_ball = SpawnBall(0, Constants.EntityDimensions.COURT_CENTRE_Y);
	}

	protected override void SpawnObjects()
	{
		m_dummyCharacter = SpawnDummyOpponentCharacter();
	}

	protected override void DestroyObjects()
	{
		//The ball will not be needed in the next TutorialState, so we can safely destroy it
		GameObject.Destroy(m_ball.gameObject);
		GameObject.Destroy(m_dummyCharacter.gameObject);
	}
}
