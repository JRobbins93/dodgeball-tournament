﻿//This script defines the third stage of the tutorial in which the player must pick up a ball from the centre line
public class PickingUpTutorialState : TutorialState
{
    public PickingUpTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		base.OnEnter();

        GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_PICKED_UP, OnBallPickedUp);
	}

	public override void OnExit()
	{
		base.OnExit();

        GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_PICKED_UP, OnBallPickedUp);
	}

    public override string GetTutorialText()
    {
        return Constants.Strings.PICKING_UP_TUTORIAL_INSTRUCTION;
    }

    private void OnBallPickedUp()
    {
		//Once the action has been performed, we can move onto the next TutorialState
		StartExitCounter();
    }

	protected override void SpawnObjects()
	{
		SpawnBall(0, Constants.EntityDimensions.COURT_CENTRE_Y);
	}

	protected override void DestroyObjects()
	{
		//Don't need to destroy the ball since the next TutorialState requires the player to be holding one
	}
}
