﻿using UnityEngine;

//This script is attached to an object that will detect when a Character has collided with it and fire an event stating as such
//It is used exclusively within the MovingTutorialState class as a target for the Character to walk towards
public class MovementTrigger : MonoBehaviour {

	private EventManager m_eventManager;

	private void Awake()
	{
		m_eventManager = GameObject.FindObjectOfType<EventManager>();
	}	

	private void OnTriggerEnter2D(Collider2D other)
	{
		//Hide the marker once it has been stepped on
        gameObject.SetActive(false);

		m_eventManager.SendEvent(Constants.EventNames.ON_MOVEMENT_TRIGGER_ACTIVATED);
	}
}
