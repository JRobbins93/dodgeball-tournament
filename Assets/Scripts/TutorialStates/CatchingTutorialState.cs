﻿using System.Collections.Generic;
using UnityEngine;

//This script defines the sixth stage of the tutorial in which the player must catch a ball thrown by an AI opponent
//The player can fail this TutorialState by:
//	Getting hit by the thrown ball
//	Failing to catch the ball before it drops to the ground
public class CatchingTutorialState : TutorialState
{
	private Ball m_ball;

	private Character m_playerCharacter;
	private Character m_opponentCharacter;

    public CatchingTutorialState(Tutorial tutorial) : base(tutorial)
    {
    }

    public override void OnEnter()
	{
		//Since no frames will have passed between exiting the last TutorialState and entering this one, the dummy Character will not yet have been deleted.
		//Therefore, we iterate through all of the Characters in the scene and check the team index to determine which is the player's
		m_playerCharacter = FindPlayerCharacter();

		base.OnEnter();

        GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_HIT_BACK_WALL, OnMissedThrow);
		GetTutorial().GetEventManager().AddListener(Constants.EventNames.ON_BALL_LOST_VELOCITY, OnMissedThrow);
	}

	public override void OnExit()
	{
		base.OnExit();

		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_CHARACTER_ELIMINATED, OnCharacterEliminated);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_HIT_BACK_WALL, OnMissedThrow);
		GetTutorial().GetEventManager().RemoveListener(Constants.EventNames.ON_BALL_LOST_VELOCITY, OnMissedThrow);
	}

    public override string GetTutorialText()
    {
        return Constants.Strings.CATCHING_TUTORIAL_INSTRUCTION;
    }

    private void OnCharacterEliminated()
    {
		//If the opposing Character has been eliminated, then the player must have caught the Ball they threw, meaning we can move onto the next TutorialState
		//If the player Character has been eliminated, they failed to catch the Ball and we must reset the TutorialState
		if(m_opponentCharacter.IsInPlay())
		{
			StartResetCounter(Constants.Strings.CATCHING_TUTORIAL_ON_PLAYER_ELIMINATED_INSTRUCTION);
		}
		else
		{
			StartExitCounter();
		}
    }

	private void OnMissedThrow()
	{
		//If the Ball hits a back wall or loses its velocity, then the player must have failed to catch the Ball, need to reset the TutorialState
		StartResetCounter(Constants.Strings.CATCHING_TUTORIAL_ON_MISSED_THROW_INSTRUCTION);
	}

	public override void Reset()
	{
		//If we're resetting this TutorialState then the player Character could have potentially been eliminated, we'll need to spawn a new one
		GameObject.Destroy(m_playerCharacter.gameObject);
		m_playerCharacter = SpawnPlayerCharacter();

		//Will need to create a new Ball upon reset since the opposing player will no longer be holding one.
		GameObject.Destroy(m_ball.gameObject);

		base.Reset();
	}

	protected override void SpawnObjects()
	{
		//Create Ball that can only be picked up by an opponent
		m_ball = SpawnBall(1, Constants.EntityDimensions.COURT_CENTRE_Y);

		//Create an AI opponent who will throw the Ball at the player
		AICoordinator coordinator = new AICoordinator();

		m_opponentCharacter = SpawnOpponentCharacter(coordinator);

		coordinator.SetCharacters(new List<Character> { m_playerCharacter, m_opponentCharacter });

		coordinator.SetBalls(new List<Ball> { m_ball });
	}

	protected override void DestroyObjects()
	{
		//We don't need to destroy the Ball upon exit since the subsequent TutorialState requires the player to hold a Ball
		GameObject.Destroy(m_opponentCharacter.gameObject);
	}
}
