﻿using UnityEngine;

//This class represents a Ball
//It governs its state, interactions with other GameObjects and also its physics behaviours
public class Ball : MonoBehaviour {

	//The different sprites to be used to visually display the current state of the Ball
	[SerializeField]
	private Sprite m_safeSprite = null;
	[SerializeField]
	private Sprite m_dangerSprite = null;
	[SerializeField]
	private Sprite m_inactiveSprite = null;

    private EventManager m_eventManager;

	//At the start of a game, the Ball can only be picked up by Characters from a specific team
	//The referenced sprite renderer indicates which team,
	//We keep a reference so that we can turn off this renderer when it is no longer needed
	[SerializeField]
	private SpriteRenderer m_teamIndicatorSpriteRenderer = null;

	private Rigidbody2D m_rigidBodyComponent;

	private SpriteRenderer m_spriteRendererComponent;

	//At the start of the game, Balls are created as children of a container transform
	//When the Ball is picked up by a Character, the Ball becomes a child of the Character's transform
	//This reference is kept so that, upon being thrown or dropped, the Ball becomes a child of the container transform once again
	private Transform m_initialParentTransform;

	//At the start of a game, the Ball can only be picked up by Characters from the team with this index
	private int m_initialPickupTeamIndex;

	//Flags that collectively define the Ball's state
	private bool m_isInPlay;
    private bool m_isHeld;
    private bool m_isOnFloor;

	//The last Character to throw the Ball, this Character will be eliminated if the Ball is caught
    private Character m_lastThrowingCharacter;

	//Variables that govern the physics behaviour at each step
	private Vector2 m_throwVelocityToApplyNextPhysicsStep;
    private bool m_flipSideOfCourtNextPhysicsStep;
    private bool m_applyDeflectionForceNextPhysicsStep;
    private bool m_applyEliminationForceNextPhysicsStep;
	private bool m_stopVelocityNextPhysicsStep;

    public void Init(int initialPickupTeamIndex)
	{
		//Set up the indicator to show which team a Character must be from to pick up the Ball at the start of the game
		m_initialPickupTeamIndex = initialPickupTeamIndex;
		m_teamIndicatorSpriteRenderer.color = Constants.Colours.TEAM_COLOURS[initialPickupTeamIndex];
	}

	private void Awake()
	{
		m_eventManager = GameObject.FindObjectOfType<EventManager>();

		m_initialParentTransform = transform.parent;

		m_rigidBodyComponent = GetComponent<Rigidbody2D>();

		m_spriteRendererComponent = GetComponent<SpriteRenderer>();

        m_isOnFloor = true;
	}

	private void Update()
	{
		//If the Ball is currently being held by a Character, we want it to follow them
		if (transform.parent != m_initialParentTransform)
		{
			transform.localPosition = Vector3.zero;
		}
	}

	private void FixedUpdate()
	{
        Vector2 positionToApply = m_rigidBodyComponent.position;
        Vector2 velocityToApply = m_rigidBodyComponent.velocity;

		if(m_stopVelocityNextPhysicsStep)
		{
			velocityToApply = Vector2.zero;
		}

		if (m_throwVelocityToApplyNextPhysicsStep != Vector2.zero)
		{
			velocityToApply = m_throwVelocityToApplyNextPhysicsStep;
		}

		if(m_flipSideOfCourtNextPhysicsStep)
		{
			//When a Ball is held for too long and given up as a dead Ball, it appears on the opposite side of the court
			positionToApply.x = 2 * Constants.EntityDimensions.COURT_CENTRE_X - m_rigidBodyComponent.position.x;

            velocityToApply = Vector2.zero;
		}

        if(m_applyDeflectionForceNextPhysicsStep)
        {
			//Upon deflection, the Ball should bounce off in the opposite direction, offset by a random angle
			float theta = Random.Range(Constants.Physics.MIN_DEFLECTION_ANGLE, Constants.Physics.MAX_DEFLECTION_ANGLE);
			velocityToApply.x *= -1;
			velocityToApply = Quaternion.AngleAxis(theta, Vector3.forward) * velocityToApply;

			//Want to scale the velocity in response to being deflected
			velocityToApply *= Constants.Physics.VELOCITY_FACTOR_PER_DEFLECTION;
		}

        if(m_applyEliminationForceNextPhysicsStep)
        {
			//If the Ball eliminated a Character, it should bounce off in a completely random direction
            float theta = Random.Range(0.0f, Mathf.PI * 2.0f);
			velocityToApply = Quaternion.AngleAxis(theta, Vector3.forward) * velocityToApply;

			//Want to scale the velocity in response to being hitting the Character
			velocityToApply *= Constants.Physics.VELOCITY_FACTOR_PER_ELIMINATION;
		}

		//If the ball has gone past the left or right wall
		if (positionToApply.x >= Constants.EntityDimensions.BALL_MAX_X || positionToApply.x <= Constants.EntityDimensions.BALL_MIN_X)
        {
			//Make Ball bounce back in the opposite horizontal direction
            velocityToApply.x = -velocityToApply.x;
            velocityToApply *= Constants.Physics.VELOCITY_FACTOR_PER_WALL_HIT;

			//Send the event as some TutorialStates need to be made aware
			m_eventManager.SendEvent(Constants.EventNames.ON_BALL_HIT_BACK_WALL);
			
			//Balls are no longer considered dangerous after hitting a back wall
			DropToFloor();
		}

		//If the ball has gone past the top or bottom wall
        if (positionToApply.y >= Constants.EntityDimensions.BALL_MAX_Y || positionToApply.y <= Constants.EntityDimensions.BALL_MIN_Y)
        {
			//Make Ball bounce back in the opposite vertical direction
			velocityToApply.y = -velocityToApply.y;            
            velocityToApply *= Constants.Physics.VELOCITY_FACTOR_PER_WALL_HIT;
        }

		//Ensure the Ball is within the court boundaries
		positionToApply.x = Mathf.Clamp(positionToApply.x, Constants.EntityDimensions.BALL_MIN_X, Constants.EntityDimensions.BALL_MAX_X);
		positionToApply.y = Mathf.Clamp(positionToApply.y, Constants.EntityDimensions.BALL_MIN_Y, Constants.EntityDimensions.BALL_MAX_Y);

		//The ball should gradually slow down after time, so apply scaling each physics step
		velocityToApply *= Constants.Physics.VELOCITY_FACTOR_PER_PHYSICS_STEP;

		//If the velocity of the ball goes below a certain magnitude, it is no longer considered to be in the air
        if(!m_isOnFloor && !m_isHeld && velocityToApply.sqrMagnitude <= Constants.Physics.FALL_TO_FLOOR_VELOCITY_THRESHOLD)
        {
			//Send the event as some TutorialStates need to be made aware
			m_eventManager.SendEvent(Constants.EventNames.ON_BALL_LOST_VELOCITY);

			DropToFloor();
        }

		//To give the Ball the appearance of motion, rotate it by an amount relative to its velocity
		transform.Rotate(Vector3.forward, velocityToApply.sqrMagnitude);

		//Apply the calculated position and velocity
        m_rigidBodyComponent.position = positionToApply;
        m_rigidBodyComponent.velocity = velocityToApply;

		//Reset the values to govern physics behaviour ahead of the next step
        m_throwVelocityToApplyNextPhysicsStep = Vector2.zero;
        m_flipSideOfCourtNextPhysicsStep = false;
        m_applyDeflectionForceNextPhysicsStep = false;
        m_applyEliminationForceNextPhysicsStep = false;
		m_stopVelocityNextPhysicsStep = false;
    }

	public int GetInitialPickupTeamIndex()
	{
		return m_initialPickupTeamIndex;
	}

	public bool IsInPlay()
	{
		return m_isInPlay;
	}

	//Called by the Character carrying the ball to mark the Ball as 'in play' upon carrying it to a back wall of the court, meaning the Ball can now be thrown
	public void SetAsInPlay()
	{
		m_isInPlay = true;
		//Set the sprite to indicate the ball is now throwable
		m_spriteRendererComponent.sprite = m_safeSprite;

		//Send the event as some TutorialStates need to be made aware
		m_eventManager.SendEvent(Constants.EventNames.ON_BALL_PUT_INTO_PLAY);
	}

    public bool IsOnFloor()
    {
        return m_isOnFloor;
    }

    public Character GetLastThrowingCharacter()
    {
		return m_lastThrowingCharacter;
    }

	//Called when the Ball is picked up by a Character
	public void OnPickedUp()
	{
		//Update state tracking variables
        m_isHeld = true;
        m_isOnFloor = false;

		//Sprite rotation is based on velocity, zero it out so Ball doesn't spin in the Character's hands
		m_stopVelocityNextPhysicsStep = true;

		//If the Ball has never been picked up before, this sprite will show which team the first Character to pick it up must be playing for
		//Once the ball has been picked up, turn this indicator off
		if (m_teamIndicatorSpriteRenderer.gameObject.activeSelf)
		{
			m_teamIndicatorSpriteRenderer.gameObject.SetActive(false);
		}

		if (!m_isInPlay)
		{
			//Indicate to the player that the ball they are carrying is not 'in play' so they understand why they are unable to throw it
			m_spriteRendererComponent.sprite = m_inactiveSprite;
		}

		//Send the event as some TutorialStates need to be made aware
		m_eventManager.SendEvent(Constants.EventNames.ON_BALL_PICKED_UP);
    }

	//Called when the Ball is thrown by a Character
	public void OnThrown(Vector2 throwVector, Character throwingCharacter)
	{
		//Ball should no longer be a child object of the carrying Character and should instead be a child or its original container
		transform.SetParent(m_initialParentTransform);

		m_throwVelocityToApplyNextPhysicsStep = throwVector;

		//Keep reference to the last throwing Character, so that if the Ball is caught, we know who to eliminate
        m_lastThrowingCharacter = throwingCharacter;

        m_isHeld = false;

		//Update sprite to show that the Ball will now eliminate any opposing Character it comes into contact with
		m_spriteRendererComponent.sprite = m_dangerSprite;
	}

	//Called when the Ball is caught by a Character
	public void OnCaught()
	{
		//Call to eliminate the throwing Character from the game
        m_lastThrowingCharacter.OnThrownBallCaught();
        m_lastThrowingCharacter = null;

        m_isHeld = true;

		//Sprite rotation is based on velocity, zero it out so Ball doesn't spin in the Character's hands
		m_stopVelocityNextPhysicsStep = true;

		//Update sprite to show it no longer dangerous
		m_spriteRendererComponent.sprite = m_safeSprite;
	}

	//Called when a Ball is deflected by a Character holding a Ball	of their own
	public void OnDeflected()
	{
		//Want to add a random deflection angle
        m_applyDeflectionForceNextPhysicsStep = true;

        m_lastThrowingCharacter = null;

		//Update sprite to show it no longer dangerous
		m_spriteRendererComponent.sprite = m_safeSprite;

		//Send the event as some TutorialStates need to be made aware
		m_eventManager.SendEvent(Constants.EventNames.ON_BALL_DEFLECTED);
	}

	//Called when the Ball has been thrown and come into contact with a Character and eliminated them
    public void OnEliminatedCharacter()
    {
		//Want to add a random angle
		m_applyEliminationForceNextPhysicsStep = true;

		//Ball should not be dangerous to Characters after eliminating one, else it is possible for one throw to potentially wipe out the entire enemy team
		DropToFloor();
    }

	//Called when the Ball has been held by a Character for too long and must be surrendered to the opposing team
	public void OnDeadBall()
	{
		//Ball should no longer be a child object of the carrying Character and should instead be a child or its original container
		transform.SetParent(m_initialParentTransform);

		//Moving the Ball to the opposing side of the court is handled during physics calculations
		m_flipSideOfCourtNextPhysicsStep = true;

		//Update state variables
        m_isHeld = false;
        m_isInPlay = true;

		DropToFloor();

		//Send the event as some TutorialStates need to be made aware
		m_eventManager.SendEvent(Constants.EventNames.ON_DEAD_BALL);
	}

	//Called when the Character holding the Ball has been eliminated
	public void OnDropped()
	{
		//Ball should no longer be a child object of the carrying Character and should instead be a child or its original container
		transform.SetParent(m_initialParentTransform);

		m_isHeld = false;

		DropToFloor();
	}

	//Marks as the Ball as being on the floor, meaning that it can be picked up once again
	private void DropToFloor()
	{
		m_isOnFloor = true;
		m_lastThrowingCharacter = null;

		//Update sprite to show it no longer dangerous
		m_spriteRendererComponent.sprite = m_safeSprite;
	}
}
