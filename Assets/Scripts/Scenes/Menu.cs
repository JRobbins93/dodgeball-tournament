﻿using UnityEngine;
using UnityEngine.SceneManagement;

//This script coordinates the behaviour of the main menu scene
//It is responsible for interpolating the different panels onto the screen smoothly and responding to button presses from the various menu items
public class Menu : MonoBehaviour {

	//How quickly panels move onto/out of the screen
	[SerializeField]
	private float m_panelMoveSpeed = 0;

	//References to each of the panels that make up the menu
	[SerializeField]
	private RectTransform m_mainPanelTransform = null;
	[SerializeField]
	private RectTransform m_creditsPanelTransform = null;
	[SerializeField]
	private RectTransform m_modeSelectPanelTransform = null;
	[SerializeField]
	private RectTransform m_tutorialPromptPanelTransform = null;

	//Reference to the currently active panel and the next panel to interpolate to (if any)
	private RectTransform m_activePanelTransform;
	private RectTransform m_nextPanelTransform;

	//Name of the scene to load once the current panel has left the screen
	private string m_sceneToLoad;

	private EventManager m_eventManager;

	private void Awake()
	{
		//Initially, the main panel should be displayed
		m_creditsPanelTransform.gameObject.SetActive(false);
		m_modeSelectPanelTransform.gameObject.SetActive(false);
		m_tutorialPromptPanelTransform.gameObject.SetActive(false);
		m_mainPanelTransform.gameObject.SetActive(true);

		m_activePanelTransform = m_mainPanelTransform;

		m_eventManager = GameObject.FindObjectOfType<EventManager>();
		m_eventManager.SendEvent(Constants.EventNames.ON_SCENE_LOADED);
	}

	private void Update()
	{
		//Check if we need to move the current panel off the screen
		if(m_nextPanelTransform != null || m_sceneToLoad != null)
		{
			//Move panels along the screen
			m_activePanelTransform.position += Vector3.right * m_panelMoveSpeed;

			if (m_nextPanelTransform != null)
			{
				m_nextPanelTransform.position += Vector3.right * m_panelMoveSpeed;
			}

			//Check whether the current panel has definitely left the viewable area of the scene
			if (m_activePanelTransform.position.x >= Screen.width * 1.5f)
			{
				if (m_sceneToLoad != null)
				{
					SceneManager.LoadScene(m_sceneToLoad);
				}
				else
				{
					//Transition has finished, can consider the next panel to be the current one
					m_activePanelTransform.gameObject.SetActive(false);
					m_activePanelTransform = m_nextPanelTransform;
					m_nextPanelTransform = null;
				}
			}
		}
	}

	//Given a panel's transform, will cause that panel to interpolate onto the screen
	private void SetNextPanelTransform(RectTransform nextPanelTransform)
	{
		//Move next panel to beyond the left edge of the screen and make it active
		m_nextPanelTransform = nextPanelTransform;
		m_nextPanelTransform.anchoredPosition = Vector3.left * Screen.width;
		m_nextPanelTransform.gameObject.SetActive(true);

		m_eventManager.SendEvent(Constants.EventNames.ON_MENU_ACTION);
	}

	//Shows the menu main menu panel
	public void OnMenuButtonClicked()
	{
		SetNextPanelTransform(m_mainPanelTransform);
	}

	//Shows the credits panel
	public void OnCreditsButtonClicked()
	{
		SetNextPanelTransform(m_creditsPanelTransform);
	}

	//Shows the game mode selection panel
	public void OnPlayButtonClicked()
	{
		SetNextPanelTransform(m_modeSelectPanelTransform);
	}

	//Launches the game scene in single player mode
	public void OnSinglePlayerButtonClicked()
	{
		Game.s_selectedGameMode = Game.GameMode.SinglePlayer;
        OnGameModeButtonClicked();
    }

	//Launches the game scene in two player - competitive mode
	public void OnTwoPlayersCompetitiveButtonClicked()
	{
		Game.s_selectedGameMode = Game.GameMode.TwoPlayerCompetitive;
        OnGameModeButtonClicked();
    }

	//Launches the game scene in two player - cooperative mode
	public void OnTwoPlayersCooperativeButtonClicked()
	{
		Game.s_selectedGameMode = Game.GameMode.TwoPlayerCooperative;
        OnGameModeButtonClicked();
	}

	//Launches the game scene in the selected mode, prompting the user to go through the tutorial if they have not previously
    private void OnGameModeButtonClicked()
    {
		if (!Tutorial.s_hasTutorialBeenCompleted)
		{
			SetNextPanelTransform(m_tutorialPromptPanelTransform);
		}
		else
		{
			m_sceneToLoad = Constants.SceneNames.GAME;			
		}

		//Don't want to show the controls panel every time the game scene is reloaded, so setting this variable from outside of that scene
		//Likewise, we want to reset the counter of how many times each team has won a game, which must be done from outside the scene
		Game.s_showControlsPanel = true;
		Game.s_numberOfGamesWon = new int[Constants.GameRules.NUMBER_OF_TEAMS];
	}

	//Launches the game scene
	public void OnSkipTutorialButtonPressed()
	{
		m_sceneToLoad = Constants.SceneNames.GAME;
	}

	//Launches the tutorial scene
	public void OnTutorialButtonClicked()
	{
		m_sceneToLoad = Constants.SceneNames.TUTORIAL;
	}

	//Closes the application
	public void OnQuitButtonClicked()
	{
		Application.Quit();
	}
}
