﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//This script coordinates the behaviour of the Game scene
//It is responsible for creating all of the GameObjects that make up a game
//It also checks whether the game has ended and if so, determining a winner and displaying this information to the user
public class Game : PauseableScene {

	//This enumaration describes the different game modes, indicating how many human players are taking part
	//and which teams their controlled Characters will be on
	public enum GameMode { SinglePlayer, TwoPlayerCompetitive, TwoPlayerCooperative };

	//Static variable defining how many human players are playing the game and which teams their Characters will be on
	//Assigned to from the Menu scene based on user input
	public static GameMode s_selectedGameMode;

	//The stage that game can be in
	//During PreGame, a counter ticks down to the start of the game
	//During Game, the game proceeds as normal
	//During PostGame, information about the winning team is displayed
	private enum GamePhase { PreGame, Game, PostGame };

	//The phase the game is currently in
	private GamePhase m_currentGamePhase;

	//If we are entering this scene from the main menu, then we want to remind the user of the game's controls
	//If, however, we are simply reloading this scene upon the end of the game, this would be obtrusive
	//So we use this static flag to indicate if controls are displayed upon entry of the scene
	public static bool s_showControlsPanel;

	//After each game, we want to display to the user the total number of games they have won/lost
	//If we enter this scene from the main menu, we want to reset these numbers
	//Whereas we do not if we have merely reloaded this scene upon the end of a game, hence this static variable
	public static int[] s_numberOfGamesWon;

	//To keep the scene hierarchy organised, Characters from each team are added as children of a container transform for their given team
	[SerializeField]
    private Transform[] m_characterContainerTransforms = null;

	//To keep the scene hierarchy organised, Balls used in the game are added as children of a container transform
	[SerializeField]
	private Transform m_ballContainerTransform = null;

	//Characters on each of the teams are created from a specific prefab, which are stored in this array
    [SerializeField]
    private GameObject[] m_characterPrefabs = null;

	//Prefab used to create the balls used in the game
	[SerializeField]
	private GameObject m_ballPrefab = null;

	//Panel upon which controls are displayed to player upon entering this scene from the main menu
    [SerializeField]
    private GameObject m_controlsPanel = null;

	//Object which contains the text informing a second human player about the controls they should use
	//Should be turned off during single player game mode
	//Positioning of these text objects is coordinate by the HorizontalLayoutGroup component on their parent GameObject
    [SerializeField]
    private GameObject m_p2ControlsTextGameObject = null;

	//The panel which is displayed upon the end of a game
	//Contains information on who won, the current win/loss count of both teams, as well as buttons to restart the game or return to the main menu
	[SerializeField]
	private GameObject m_gameOverPanel = null;

	//Panel that houses the timers for how long the current game has been going on for and the countdown at the start of a game
	[SerializeField]
	private GameObject m_timerPanel = null;

	//Text that informs the user of which team has won the game, text content is assigned at runtime based on the winning team
	[SerializeField]
	private Text m_winningTeamText = null;

	//Text objects that show how many games each team has won, content is assigned at runtime based on the the winning team
	[SerializeField]
	private Text[] m_winCounterTexts = null;

	//Text that counts down from 3 to 1 at the start of a game to avoid a jarring transition to the game scene
	[SerializeField]
	private Text m_countDownText = null;

	//Text that displays how long the current game has been going on for
	[SerializeField]
	private Text m_gameTimerText = null;

	//Keep track of all Balls and Characters within the scene
	private List<Ball> m_balls;
    private List<Character> m_characters;

	//As Characters are eliminated from the game, they are added to the queue associated with their team
	//When another Character on their team catches a Ball thrown by an opponent, the Character at the front of the queue for the catching player's team will be returned to the game
    private Queue<Character>[] m_eliminatedCharacters;

	//The AICoordinator who sets the targets for all AI controlled Characters within the game
	private AICoordinator m_coordinator;

	//How long the game has been going on for, also used as the value governing the countdown timer before a game begins
	private float m_timer;

	//How long since the game ended, used as a delay before showing the game over screen
	//This avoids a jarring user expierence and also provides time for the winning Character's victory animation to play
	private float m_gameOverTimer;

	protected override void Awake()
    {
		base.Awake();

		m_balls = new List<Ball>();
        m_characters = new List<Character>();

		//Initialise each teams queue for eliminated Characters to return to the game
        m_eliminatedCharacters = new Queue<Character>[Constants.GameRules.NUMBER_OF_TEAMS];
        for (int teamIndex = 0; teamIndex < m_eliminatedCharacters.Length; teamIndex++)
        {
            m_eliminatedCharacters[teamIndex] = new Queue<Character>();
        }

		m_coordinator = new AICoordinator();

		m_gameOverPanel.SetActive(false);
		m_timerPanel.SetActive(true);

		//Update the control panel to only show the controls for a second human player if there is definitely more than one player
		m_p2ControlsTextGameObject.SetActive(s_selectedGameMode != GameMode.SinglePlayer);
		m_controlsPanel.SetActive(s_showControlsPanel);
		s_showControlsPanel = false;

		m_countDownText.gameObject.SetActive(true);
		m_gameTimerText.gameObject.SetActive(false);

		m_currentGamePhase = GamePhase.PreGame;
	}

    protected override void Start()
    {
		base.Start();

        CreateTeams();
		CreateBalls();
	}

	protected override void Update()
	{
		base.Update();

		if(m_controlsPanel.activeSelf)
		{
			//Don't want to progress with the game if the user has yet to dismiss the controls panel
			return;
		}

		if (m_currentGamePhase == GamePhase.PostGame)
		{
			//If the game has finished, we count down the timer before displaying the results screen
			m_gameOverTimer -= Time.unscaledDeltaTime;

			if(m_gameOverTimer <= 0.0f)
			{
				m_gameOverPanel.SetActive(true);
			}
			return;
		}

		m_timer += Time.deltaTime;

		if(m_currentGamePhase == GamePhase.Game)
		{
			m_gameTimerText.text = Mathf.Floor(m_timer).ToString();
		}
		else
		{
			//We want to show a countdown to the start of the game to avoid a jarring user experience
			int countdownValue = (int)Mathf.Ceil(Constants.GameRules.STARTING_COUNTDOWN_LENGTH - m_timer);
			
			if(countdownValue == 0)
			{
				//Count down has expired, the game can begin
				m_currentGamePhase = GamePhase.Game;
				m_countDownText.gameObject.SetActive(false);
				m_gameTimerText.gameObject.SetActive(true);
				//Reset the timer since this is now used to see how long game has been going on for
				m_timer = 0.0f;
			}
			else
			{
				m_countDownText.text = countdownValue.ToString();
			}
		}

		CheckForGameOver();
	}

	//Returns value indicating if the game has started yet
	public bool HasStarted()
	{
		return m_currentGamePhase != GamePhase.PreGame;
	}

	//Button callback triggered when the player selects the button to dismiss the control instruction message at the start of a game
    public void OnOkPressed()
    {
        m_controlsPanel.SetActive(false);
    }

	private void CreateTeams()
    {
		for (int teamIndex = 0; teamIndex < Constants.GameRules.NUMBER_OF_TEAMS; teamIndex++)
		{
			//Teams start the back walls of the court
            float teamXPos = (teamIndex == 0) ? Constants.EntityDimensions.CHARACTER_MIN_X[0] : Constants.EntityDimensions.CHARACTER_MAX_X[1];

			for (int characterIndex = 0; characterIndex < Constants.GameRules.NUMBER_OF_CHARACTERS_PER_TEAM; characterIndex++)
			{
				GameObject characterObject = GameObject.Instantiate(m_characterPrefabs[teamIndex], m_characterContainerTransforms[teamIndex]);
                
				//Don't want players on the same team to collide with each other, so move them to a specific layer which ignores collisions between two objects on it
				characterObject.layer = LayerMask.NameToLayer(Constants.LayerNames.TEAMS[teamIndex]);

				//Want to evenly space Characters along their back wall
                float characterYPos = Mathf.Lerp(Constants.EntityDimensions.CHARACTER_MIN_Y, Constants.EntityDimensions.CHARACTER_MAX_Y, (characterIndex + 0.5f) / Constants.GameRules.NUMBER_OF_CHARACTERS_PER_TEAM);

				characterObject.transform.position = new Vector2(teamXPos, characterYPos);

				Character character = characterObject.GetComponent<Character>();

				if(characterIndex == 0 && teamIndex == 0)
				{
					//The first character on the first team is always controlled by a human player
					character.Init(new KeyboardCharacterCommandIssuer(0), teamIndex, this);
				}
				else if((s_selectedGameMode == GameMode.TwoPlayerCompetitive && characterIndex == 0 && teamIndex == 1)
					|| (s_selectedGameMode == GameMode.TwoPlayerCooperative && characterIndex == 1 && teamIndex == 0))
				{
					//In two player competitive mode, the first Character on the second team is controlled by the second human player
					//In two player cooperative mode, the second human player controls the second Character on the first team
					character.Init(new KeyboardCharacterCommandIssuer(1), teamIndex, this);
				}
				else
				{
					//All other Character are controllers by the AI coordinator
					character.Init(new AICharacterCommandIssuer(character, m_coordinator), teamIndex, this);
				}
				
				m_characters.Add(character);
			}
		}

		//Inform the AI coordinator of all the Characters within the game to inform its decision making
		m_coordinator.SetCharacters(m_characters);
    }

	private void CreateBalls()
	{
		for(int ballIndex = 0; ballIndex < Constants.GameRules.NUMBER_OF_BALLS; ballIndex++)
		{
			GameObject ballObject = GameObject.Instantiate(m_ballPrefab, m_ballContainerTransform);

			//Want to evenly space the balls along the centre line of the court
			float ballYPos = Mathf.Lerp(Constants.EntityDimensions.BALL_MIN_Y, Constants.EntityDimensions.BALL_MAX_Y, (ballIndex + 0.5f) / Constants.GameRules.NUMBER_OF_BALLS);
			ballObject.transform.position = new Vector2(Constants.EntityDimensions.COURT_CENTRE_X, ballYPos);

			Ball ball = ballObject.GetComponent<Ball>();

			//At the start of play, each Ball can only be picked by a Character from a certain team
			//We want to alternate Balls to be picked up by each team
			int teamIndex = ballIndex % 2;
			ball.Init(teamIndex);

			m_balls.Add(ball);
		}

		//Inform the AI coordinator of all the Balls within the game to inform its decision making
		m_coordinator.SetBalls(m_balls);
	}

    public void OnCharacterEliminated(Character character)
    {
        int characterTeamIndex = character.GetTeamIndex();
		//Place the eliminated Character at the back of the their team's queue of Characters to come back into the game upon a Ball being caught by someone on their team
        m_eliminatedCharacters[characterTeamIndex].Enqueue(character);
    }

	//Brings an eliminated Character for the given team back into the Game upon one of their teammates catching a Ball thrown by an opponent
    public void ReviveCharacter(int teamIndex)
    {
		//Check if there is even an eliminated Character for us to revive
        if (m_eliminatedCharacters[teamIndex].Count > 0)
        {
			//Characters return to the game in the order that they were eliminated, so get the Character of the front of the given team's queue
			//And remove them from the queue
            Character characterToRevive = m_eliminatedCharacters[teamIndex].Dequeue();

			characterToRevive.OnRevived();
        }
    }

	protected override bool CanBePaused()
	{
		//User can pause the game so long as the results screen or controls prompts are not currently showing
		return m_currentGamePhase != GamePhase.PostGame && !m_controlsPanel.activeSelf;
	}

	//Checks to see if the game has been won by any team, and if so, displays UI informing the user of the outcome 
	private void CheckForGameOver()
	{
		for (int teamIndex = 0; teamIndex < m_eliminatedCharacters.Length; teamIndex++)
		{
			//If all Characters on the team have been eliminated, they must have lost
			if (m_eliminatedCharacters[teamIndex].Count == Constants.GameRules.NUMBER_OF_CHARACTERS_PER_TEAM)
			{
				//If the current team has lost, then the other team must have won
				int winningTeamIndex = Constants.GameRules.NUMBER_OF_TEAMS - 1 - teamIndex;

				//Update the counters so the user knows how many games each team has won/lost since entering the Game scene from the main menu
				s_numberOfGamesWon[winningTeamIndex]++;

				//Update the UI displaying the win counts for both teams
				m_winCounterTexts[teamIndex].text = s_numberOfGamesWon[teamIndex].ToString();
				m_winCounterTexts[winningTeamIndex].text = s_numberOfGamesWon[winningTeamIndex].ToString();

				//Display message informing user of which team won, colouring it as appropriate
				m_winningTeamText.text = string.Format(Constants.Strings.GAME_OVER_MESSAGES[winningTeamIndex]);
				m_winningTeamText.color = Constants.Colours.TEAM_COLOURS[1 - teamIndex];

				GetEventManager().SendEvent(Constants.EventNames.ON_GAME_ENDED);

				m_currentGamePhase = GamePhase.PostGame;
				//Begin the countdown before displaying the result UI
				//This makes for a less jarring user experience and gives a chance for the Character's victory animation to play
				m_gameOverTimer = Constants.GameRules.GAME_OVER_TIMER_LENGTH;

				for (int characterIndex = 0; characterIndex < m_characters.Count; characterIndex++)
				{
					Character currentCharacter = m_characters[characterIndex];

					if (currentCharacter.GetTeamIndex() == winningTeamIndex && currentCharacter.IsInPlay())
					{
						//All surviving Characters on the winning team should play their victory animation
						currentCharacter.OnVictory();
					}
				}
			}
		}
	}
}
