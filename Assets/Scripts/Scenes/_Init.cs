﻿using UnityEngine;
using UnityEngine.SceneManagement;

//This script coordinates the _Init scene
//The scene is responsible for holding the objects that will persist across the lifetime of the game
//This scene immediately transitions into the menu scene and the user has no interaction with this scene
public class _Init : MonoBehaviour {

	//Reference to the object that will not be destroyed when a new scene is loaded
	[SerializeField]
	private GameObject m_persistentObject = null;

	private void Start()
    {
		DontDestroyOnLoad(m_persistentObject);
		
		SceneManager.LoadScene(Constants.SceneNames.MENU);
    }
}
