﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//This script coordinates the behaviour of the tutorial scene
public class Tutorial : PauseableScene {

	//Public flag that is set to true once the final TutorialState has been completed
	//If it is false when the user tries to enter a Game, they will be prompted to go through the Tutorial instead
	public static bool s_hasTutorialBeenCompleted;

	//Prefab for the Character controlled by the player during the Tutorial
	[SerializeField]
	private GameObject m_playerCharacterPrefab = null;

	//Prefab fpr the Character on the opposing team during the Tutorial
	[SerializeField]
	private GameObject m_opponentCharacterPrefab = null;

	//Prefab for the Balls used during the Tutorial
    [SerializeField]
    private GameObject m_ballPrefab = null;

	//Prefab for the movement triggers that are used during the MovingTutorialState
    [SerializeField]
    private GameObject m_movementTriggerPrefab = null;

	//Panel which appears when a Tutorial message is being displayed
	[SerializeField]
    private GameObject m_tutorialMessagePanel = null;

	//The message that is displayed fullscreen to the user to instruct them on what they must do next or where they may have made a mistake
	[SerializeField]
    private Text m_tutorialMessageText = null;

	//Button that the user can press to make the current Tutorial message reappear
	[SerializeField]
	private GameObject m_helpButtonGameObject = null;

	//The TutorialStates that the user must progress through that define the flow of the Tutorial 
	private TutorialState[] m_states;

	//Variables for keeping track of user's progress through Tutorial
	private int m_currentStateIndex;
	private TutorialState m_currentState;	

	protected override void Awake()
	{
		base.Awake();

		//Create objects for each of the different TutorialStates in the correct order
        m_states = new TutorialState[]
		{
            new MovingTutorialState(this),
            new DodgingTutorialState(this),
            new PickingUpTutorialState(this),
            new PuttingBallIntoPlayTutorialState(this),
            new ThrowingTutorialState(this),
            new CatchingTutorialState(this),
            new DeflectingTutorialState(this),
        };

        m_currentState = m_states[0];
    }

	protected override void Start()
	{
		base.Start();
		//Go straight into the first TutorialState
		m_currentState.OnEnter();
		//Update the message displayed to the user so they know what to do during this TutorialState
		m_tutorialMessageText.text = m_currentState.GetTutorialText();
	}

	protected override void Update()
    {
		base.Update();

		if (m_currentState != null)
		{
			//If the current TutorialState has not been passed or failed, then we don't need to do anything
			//If is has been, then we need to wait for the counter to expire to avoid a jarring experience for the user
			if (m_currentState.IsExitCounterTicking())
			{
				m_currentState.DecreaseExitCounter(Time.deltaTime);

				if (m_currentState.GetExitCounter() <= 0.0f)
				{
					//Counter has expired so we can move onto the next TutorialState
					MoveToNextState();
				}
			}
			else if(m_currentState.IsResetCounterTicking())
			{
				m_currentState.DecreaseResetCounter(Time.deltaTime);

				if(m_currentState.GetResetCounter() <= 0.0f)
				{
					//Counter has expired so we can reset the current TutorialState
					m_currentState.Reset();
				}
			}
		}
    }

	protected override void TogglePaused()
	{
		base.TogglePaused();

		//Want to hide the 'help' button if the pause menu is showing
		m_helpButtonGameObject.SetActive(!IsPaused());
	}

	public GameObject GetPlayerCharacterPrefab()
	{
		return m_playerCharacterPrefab;
	}

	public GameObject GetOppoentPlayerPrefab()
	{
		return m_opponentCharacterPrefab;
	}

    public GameObject GetBallPrefab()
    {
        return m_ballPrefab;
    }

    public GameObject GetMovementTriggerPrefab()
    {
        return m_movementTriggerPrefab;
    }

	//Displays a full screen message to inform the user what they should do during the current TutorialState
	//Or if they have failed the current TutorialState, the message should describe what they have done wrong
    public void ShowTutorialMessage(string message)
    {
        m_tutorialMessagePanel.SetActive(true);
		m_helpButtonGameObject.SetActive(false);
		m_tutorialMessageText.text = message;

		//Freeze the game while the tutorial message is being displayed
        Time.timeScale = 0.0f;
    }

	//Callback for when the 'OK' button on the Tutorial message panel has been pressed
    public void OnOkPressed()
    {
        m_tutorialMessagePanel.SetActive(false);

		//Restart the game now the message has been dismissed
        Time.timeScale = 1.0f;

		//Check if the user has just dismissed the message that is displayed after passing the final TutorialState and would therefore need to return to the Menu scene
        if(m_currentStateIndex == m_states.Length)
        {
			//Setting this flag to true will mean the user is no longer prompted to go through the Tutorial when they attempt to play a Game from the Menu
			s_hasTutorialBeenCompleted = true;

            SceneManager.LoadScene(Constants.SceneNames.MENU);
        }

		m_helpButtonGameObject.SetActive(true);
    }

	//Callback for when the 'help' button is pressed to re-show the current TutorialStates instructional message
	public void OnHelpPressed()
	{
		ShowTutorialMessage(m_currentState.GetTutorialText());
	}

	private void MoveToNextState()
	{
		m_currentStateIndex++;

		//Perform cleanup behaviour for the state that the user has just completed
		if (m_currentState != null)
		{
			m_currentState.OnExit();
		}

		if (m_currentStateIndex == m_states.Length)
		{
			//If the user has completed the final TutorialState then display the message informing them of this fact
            ShowTutorialMessage(Constants.Strings.FINISHED_TUTORIAL_MESSAGE);
            m_currentState = null;
			return;
		}

		//Iterate on to the next state
		TutorialState nextState = m_states[m_currentStateIndex];

		m_currentState = nextState;

		//Perform any initialisation behaviour for the new TutorialState
		m_currentState.OnEnter();

		//Display message to user instructing them what to do in this new TutorialState
        m_tutorialMessageText.text = m_currentState.GetTutorialText();
	}

	protected override bool CanBePaused()
	{
		//Disallow the scene to be paused if there is an instructional message displayed on the screen
		return !m_tutorialMessagePanel.activeSelf;
	}
}
