﻿using UnityEngine;
using UnityEngine.SceneManagement;

//This script is an abstract class governing the ability for a scene to be paused and display a pause menu
//It is inherited from by the Game and Tutorial classes
public abstract class PauseableScene : MonoBehaviour
{
	//Reference to the pause menu
	[SerializeField]
	private GameObject m_pauseMenuPanel = null;

	private EventManager m_eventManager;

	//Abstract method that checks whether it is suitable for the scene to be paused at this moment
	protected abstract bool CanBePaused();

	public EventManager GetEventManager()
	{
		return m_eventManager;
	}

	//Checks whether the scene is currently paused
	public bool IsPaused()
	{
		return Time.timeScale == 0.0f;
	}

	protected virtual void Awake()
	{
		m_eventManager = GameObject.FindObjectOfType<EventManager>();
		m_eventManager.SendEvent(Constants.EventNames.ON_SCENE_LOADED);
	}

	protected virtual void Start()
	{
		//By default, scene is not paused
		Time.timeScale = 1.0f;
		m_pauseMenuPanel.SetActive(false);
	}

	protected virtual void Update()
	{
		//Check for key press to trigger pause if appropriate
		if(CanBePaused() && (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P)))
		{
			TogglePaused();
		}
	}

	//If the scene is currently paused, unpaused it, and vice versa
	protected virtual void TogglePaused()
	{
		//TimeScale of 1 is normal play, timeScale of 0 is completely paused
		Time.timeScale = (1.0f - Time.timeScale);

		m_pauseMenuPanel.SetActive(IsPaused());

		m_eventManager.SendEvent(Constants.EventNames.ON_MENU_ACTION);
	}

	//Unpauses the scene from a paused state
	public void OnResumeButtonPressed()
	{
		TogglePaused();
	}

	//Relaunches the currently loaded scene
	public void OnRestartButtonPressed()
	{
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	//Launches the menu scene
	public void OnMenuButtonPressed()
	{
		SceneManager.LoadScene(Constants.SceneNames.MENU);
	}
}
