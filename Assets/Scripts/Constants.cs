﻿using UnityEngine;

//This class serves as a repository for any hardcoded values that may be used throughout the game, divided into sections through the use of subclasses
//The purpose of this class is to make editing such values easier and to ensure they are only ever defined in one location
public static class Constants {

	//Names of the scenes within the project
    public static class SceneNames
    {
        public const string GAME = "Game";
        public const string TUTORIAL = "Tutorial";
        public const string MENU = "Menu";
    }

	//Names of events that can be listened to and sent through the EventManager
    public static class EventNames
    {
		public const string ON_MENU_ACTION = "OnMenuAction";
		public const string ON_SCENE_LOADED = "OnSceneLoaded";

        public const string ON_GAME_ENDED = "OnGameEnded";

        public const string ON_BALL_PICKED_UP = "OnBallPickedUp";
        public const string ON_BALL_DEFLECTED = "OnBallDeflected";
        public const string ON_BALL_PUT_INTO_PLAY = "OnBallPutIntoPlay";
        public const string ON_DEAD_BALL = "OnDeadBall";
		public const string ON_BALL_HIT_BACK_WALL = "OnBallHitBackWall";
		public const string ON_BALL_LOST_VELOCITY = "OnBallLostVelocity";

        public const string ON_CHARACTER_DODGED = "OnCharacterDodged";
        public const string ON_CHARACTER_ELIMINATED = "OnCharacterEliminated";

		public const string ON_MOVEMENT_TRIGGER_ACTIVATED = "OnMovementTriggerActivated";
    }

	//Names of the different axes used to get human player input
    public static class AxisNames
    {
        public const string HORIZONTAL_MOVEMENT = "HorizontalMovement";
        public const string VERTICAL_MOVEMENT = "VerticalMovement";
        public const string HORIZONTAL_AIM = "HorizontalAim";
        public const string VERTICAL_AIM = "VerticalAim";
        public const string DODGE = "Dodge";
        public const string PICKUP = "Pickup";
        public const string THROW = "Throw";
        public const string CATCH = "Catch";
        public const string DEFLECT = "Deflect";
        public const string PAUSE = "Pause";
    }

	//Names of the different physics layers used so that Balls thrown by a player from one team does not collide with their teammates
    public static class LayerNames
    {
        public static readonly string[] TEAMS  = { "Blue Team", "Red Team" };
		public const string BACK_WALL = "BackWall";
    }

	//Names of parameters used by the Unity animator for Characters
	public static class CharacterAnimationParameters
	{
		public const string IS_RUNNING = "IsRunning";
		public const string ON_ELIMINATED = "OnEliminated";
		public const string ON_THROW = "OnThrow";
		public const string ON_VICTORY = "OnVictory";
		public const string ON_REVIVED = "OnRevived";
	}

	//Values that govern the flow and structure of the game
    public static class GameRules
    {
        public const int NUMBER_OF_CHARACTERS_PER_TEAM = 3;
        public const int NUMBER_OF_TEAMS = 2;
        public const int NUMBER_OF_BALLS = 6;
        public const float TIME_UNTIL_DEAD_BALL = 10.0f;
        public const float ACTION_COOLDOWN = 1.0f;
        public const float STARTING_COUNTDOWN_LENGTH = 3.0f;
		public const float GAME_OVER_TIMER_LENGTH = 2.0f;
    }

	//Values used within physics calculations
    public static class Physics
    {
        public const float VELOCITY_FACTOR_PER_PHYSICS_STEP = 0.994f;
        public const float VELOCITY_FACTOR_PER_WALL_HIT = 0.95f;
        public const float VELOCITY_FACTOR_PER_DEFLECTION = 0.7f;
        public const float VELOCITY_FACTOR_PER_ELIMINATION = 0.6f;
        public const float FALL_TO_FLOOR_VELOCITY_THRESHOLD = 1.0f;

		public const float MAX_DEFLECTION_ANGLE = -30.0f;
        public const float MIN_DEFLECTION_ANGLE = -MAX_DEFLECTION_ANGLE;
	}

	//Values describing the size of different objects in the game, used for positioning calculations
    public static class EntityDimensions
    {
        public const float COURT_CENTRE_X = 0.0f;
        public const float COURT_CENTRE_Y = -1.7f;
        public const float COURT_WIDTH = 13.0f;
        public const float COURT_HEIGHT = 7.4f;
        public const float COURT_HALF_WIDTH = COURT_WIDTH / 2.0f;
        public const float COURT_HALF_HEIGHT = COURT_HEIGHT / 2.0f;
		public const float COURT_QUATER_WIDTH = COURT_WIDTH / 4.0f;
		public const float COURT_QUATER_HEIGHT = COURT_HEIGHT / 4.0f;
		public const float COURT_MIN_X = COURT_CENTRE_X - COURT_HALF_WIDTH;
        public const float COURT_MAX_X = COURT_CENTRE_X + COURT_HALF_WIDTH;
        public const float COURT_MIN_Y = COURT_CENTRE_Y - COURT_HALF_HEIGHT;
        public const float COURT_MAX_Y = COURT_CENTRE_Y + COURT_HALF_HEIGHT;

        public const float CENTRE_LINE_WIDTH = 0.54f;
        public const float CENTRE_LINE_HALF_WIDTH = CENTRE_LINE_WIDTH / 2.0f;

        public const float BALL_RADIUS = 0.375f;
        public const float BALL_MIN_X = COURT_MIN_X + BALL_RADIUS;
        public const float BALL_MAX_X = COURT_MAX_X - BALL_RADIUS;
        public const float BALL_MIN_Y = COURT_MIN_Y + BALL_RADIUS;
        public const float BALL_MAX_Y = COURT_MAX_Y - BALL_RADIUS;

        public const float CHARACTER_WIDTH = 1.32f;
        public const float CHARACTER_HEIGHT = 1.50f;
        public const float CHARACTER_HALF_WIDTH = CHARACTER_WIDTH / 2.0f;
        public const float CHARACTER_HALF_HEIGHT = CHARACTER_HEIGHT / 2.0f;

        public static readonly float[] CHARACTER_MIN_X = { COURT_MIN_X + CHARACTER_HALF_WIDTH, COURT_CENTRE_X + CENTRE_LINE_HALF_WIDTH + CHARACTER_HALF_WIDTH };
        public static readonly float[] CHARACTER_MAX_X = { COURT_CENTRE_X - CENTRE_LINE_HALF_WIDTH - CHARACTER_HALF_WIDTH, COURT_MAX_X - CHARACTER_HALF_WIDTH };
        public const float CHARACTER_MIN_Y = COURT_MIN_Y + CHARACTER_HALF_HEIGHT;
        public const float CHARACTER_MAX_Y = COURT_MAX_Y - CHARACTER_HALF_HEIGHT;
    }

	//Constants that govern AI behaviour calculations
    public static class AI
    {
        public const float TARGET_POSITION_PROXIMITY_TOLERANCE = 1.2f;
        public const float MAX_ERROR_ANGLE = 30.0f;
        public const float MIN_ERROR_ANGLE = -MAX_ERROR_ANGLE;
    }

	//Strings that appear throughout the game
    public static class Strings
    {
        public static readonly string[] GAME_OVER_MESSAGES = { "Blue Team Wins!", "Red Team Wins!" };

        public const string PLAYER_AXIS_FORMAT = "{0}_P{1}";
        public const string PLAYER_INDICATOR_TEXT_FORMAT = "P{0}";

        public const string MOVING_TUTORIAL_INSTRUCTION = "Use the W,A,S,D keys to move your character up, down, left and right.\n\nMove into each of the markers you see around the court.";

		public const string DODGING_TUTORIAL_INSTRUCTION = "You can dodge out of the way of incoming balls by pressing LEFT SHIFT as you move.\n\nTry perfoming a dodge now.";

		public const string PICKING_UP_TUTORIAL_INSTRUCTION = "Approach the ball and press SPACE to pick it up.\n\nAt the start of a real game, you can only pick up the balls marked with your team's colour.";

		public const string PUTTING_BALL_INTO_PLAY_INSTRUCTION = "Before you can throw the ball, you'll need to carry it to the back wall in order to put it 'into play'.\n\nThe ball you're holding will be greyed out until it is put 'into play'.\n\nRemember that if you hold onto a ball for too long, it will be surrendered to the other team.";
		public const string PUTTING_BALL_INTO_PLAY_TUTORIAL_ON_DEAD_BALL_INSTRUCTION = "You will surrender the ball to the other team if you hold it for too long!\n\nPress SPACE to pick up the ball and carry it to the back wall in order to put it 'into play'.";
		
		public const string THROWING_TUTORIAL_INSTRUCTION = "With the ball now 'in play' you can press SPACE to throw the ball in the direction that you are currently moving.\n\nTry eliminating the opposing player on the other side of the court.";
		public const string THROWING_TUTORIAL_ON_MISSED_THROW_INSTRUCTION = "You just missed him! Try again!\n\nRemember you'll need to press SPACE to pick up the ball, then move to the back wall to put it 'into play', before pressing SPACE to throw the ball in the direction you're moving.";
		public const string THROWING_TUTORIAL_ON_DEAD_BALL_INSTRUCTION = "You will surrender the ball to the other team if you hold it for too long!\n\nRemember you'll need to press SPACE to pick up the ball, then move to the back wall to put it 'into play', before pressing SPACE to throw the ball in the direction you're moving.";

		public const string CATCHING_TUTORIAL_INSTRUCTION = "Your opponents can throw balls of their own! When a ball has its spikes out, stay away. If it hits you, you'll be eliminated.\n\nIf you aren't holding a ball, and are close to a ball thrown by an opponent, press LEFT CTRL to catch it before it hits the ground.\n\nDoing so will eliminate the opponent who threw the ball and return the most recently eliminated player from your team into the game.\n\nTry catching the ball thrown by your opponent now.";
		public const string CATCHING_TUTORIAL_ON_MISSED_THROW_INSTRUCTION = "You didn't manage to catch the ball!\n\nYou'll need to get close to the ball before it hits the ground and press LEFT CTRL to catch it.";
		public const string CATCHING_TUTORIAL_ON_PLAYER_ELIMINATED_INSTRUCTION = "The ball hit you! Lucky this isn't a real game else you'd have been eliminated.\n\nYou'll need to get close to the ball before it hits the ground and press LEFT CTRL to catch it.";

		public const string DEFLECTING_TUTORIAL_INSTRUCTION = "If you are holding a ball while near a ball thrown by an opponent, press LEFT CTRL to deflect it away from you.";
		public const string DEFLECTING_TUTORIAL_ON_MISSED_THROW_INSTRUCTION = "You didn't manage to deflect the ball!\n\nPick up the ball from the ground, get close to the ball thrown by your opponent before it hits the ground and press LEFT CTRL in order to deflect it.";
		public const string DEFLECTING_TUTORIAL_ON_PLAYER_ELIMINATED_INSTRUCTION = "The ball hit you! Lucky this isn't a real game else you'd have been eliminated.\n\nPick up the ball from the ground, get close to the ball thrown by your opponent before it hits the ground and press LEFT CTRL in order to deflect it.";
		public const string DEFECTING_TUTORIAL_ON_OPPONENT_ELIMINATED_INSTRUCTION = "You're meant to be deflecting the ball, not eliminating your opponent!\n\nPick up the ball from the ground, get close to the ball thrown by your opponent before it hits the ground and press LEFT CTRL in order to deflect it.";
		public const string DEFLECTING_TUTORIAL_ON_DEAD_BALL_INSTRUCTIONS = "You will surrender the ball to the other team if you hold it for too long!\n\nPick up the ball from the ground, get close to the ball thrown by your opponent before it hits the ground and press LEFT CTRL in order to deflect it.";

		public const string FINISHED_TUTORIAL_MESSAGE = "Your training is complete!\n\nLooks like you're ready to take on the challenge of a real game. Remember you can always come back here to brush up on the basics!\n\nGood luck!";
    }

	//Values that govern the flow of the Tutorial scene
	public static class Tutorial
	{
		public const int NUMBER_OF_MOVEMENT_TRIGGERS_TO_sPAWN = 6;
		public const float MOVEMENT_TRIGGER_LAYOUT_RADIUS = EntityDimensions.COURT_WIDTH / 5.0f;
		public const float STATE_EXIT_TIMER = 1.0f;
		public const float STATE_RESET_TIMER = 1.0f;
	}

	//Values that govern the audio throughout the game
    public static class Audio
    {
        public const float MUSIC_FADE_OUT_TIME_ON_SCENE_LOAD = 1.0f;
        public const float MUSIC_FADE_IN_TIME_ON_SCENE_LOAD = 2.0f;

		public const float MUSIC_MAX_VOLUME = 0.4f;
    }

	//Values that define colours that appear within the game
	public static class Colours
	{
        public static readonly Color[] TEAM_COLOURS = { new Color(18.0f / 255.0f, 94.0f / 255.0f, 152.0f / 255.0f), new Color(166.0f / 255.0f, 11.0f / 255.0f, 10.0f / 255.0f) };
	}
}
