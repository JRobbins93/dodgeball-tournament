  ____            _            _           _ _   _____                                                 _   
 |  _ \  ___   __| | __ _  ___| |__   __ _| | | |_   _|__  _   _ _ __ _ __   __ _ _ __ ___   ___ _ __ | |_ 
 | | | |/ _ \ / _` |/ _` |/ _ \ '_ \ / _` | | |   | |/ _ \| | | | '__| '_ \ / _` | '_ ` _ \ / _ \ '_ \| __|
 | |_| | (_) | (_| | (_| |  __/ |_) | (_| | | |   | | (_) | |_| | |  | | | | (_| | | | | | |  __/ | | | |_ 
 |____/ \___/ \__,_|\__, |\___|_.__/ \__,_|_|_|   |_|\___/ \__,_|_|  |_| |_|\__,_|_| |_| |_|\___|_| |_|\__|
                    |___/                                                                                  
--------------------------------------------------------------------------------------------------------------------------------
NOTES ON THE CODE
--------------------------------------------------------------------------------------------------------------------------------
--------Where To Start--------
When approaching the codebase for this project for the first time, the most important classes to look at are those contained within the 'Scenes' folder.
These scripts govern the flow through the different scenes within the game.

Next, the Character and Ball classes are the two most important entities within the game itself.

Then, the 'Character' folder contains scripts handling AI and player input.

The 'TutorialStates' folder contains the classes that control the user's progression through the tutorial.

Finally the 'Managers' folder contains the scripts that are persistent throughout all scenes and handle the audio and event systems.

--------Editor Assigned Variables--------
Most of the classes in the project contain members variables which are adorned with a "[SerializeField]" attribute.
This attribute allows for the associated variable to be assigned to in the Unity editor, without making that variable public and breaking encapsulation.
These variables have a default or null value assigned in the code in order to avoid compiler warnings about unassigned variables.

--------------------------------------------------------------------------------------------------------------------------------
NOTES ON THE UNITY PROJECT
--------------------------------------------------------------------------------------------------------------------------------
--------Scenes--------
_Init
    This is the first scene entered into upon loading the application. Its sole purpose is to set up the GameObjects that persist across all other scenes, before loading immediately into the Menu scene.
    If running the game in the Unity Editor, unexpected behaviour may occur if this is not the loaded scene upon pressing play.
    
Menu
    From this scene the user can launch a game, enter the tutorial, or quit the application.

Tutorial
    This scene is where the user is taught the mechanics and controls of the game.

Game
    This is the scene where the main loop of the game takes place. 


--------Physics Layers--------
Blue Team / Red Team
    Each Character in the game is put on the physics layer corresponding to their team.
    Collisions are not triggered between two objects on these layers meaning that Characters on the same team do not collide with each other.

BackWall
    The left and right sides of the court exist on their own layer.
    Upon colliding with an object on this layer, any Balls that a Character is holding will be put into play.


--------Animation--------
For each character animation, there are two sets of sprites, colour coded for the two different teams in the game.
For each of these sets of animations there is an animator controller which can be viewed in the Unity editor.


--------------------------------------------------------------------------------------------------------------------------------
CREDITS
--------------------------------------------------------------------------------------------------------------------------------
--------Programming / Design--------
All programming and design work was carried out by myself, Joe Robbins.


--------Artwork / Animation--------
All artwork and animation contained within the game was created by Benjamin Nixon, with the exception of the following files, which were created by myself, Joe Robbins:
    MovementTrigger.png
    BallTeamIndicator.png
    PlayerIndicator.png


--------Font--------
The font used within the game, "Knight's Quest", was created by Gem Fonts and downloaded from dafont.com.


--------Music--------
"Heroic Age" / "Master of the Feast" / "Darkling" by Kevin MacLeod (incompetech.com) 
Licensed under Creative Commons: By Attribution 3.0
http://creativecommons.org/licenses/by/3.0/


--------Sound Effects--------
"Fanfare" by bone666138 / "Wilhelm Scream" by Syna-Max / "Whoosh Push" by Speedenza (freesound.org)
Licensed under Creative Commons: By Attribution-NonCommerical 3.0
https://creativecommons.org/licenses/by-nc/3.0/legalcode


--------------------------------------------------------------------------------------------------------------------------------
RESOLUTION
--------------------------------------------------------------------------------------------------------------------------------
The game only supports the resolution of 1920 x 1080 and runs in a fullscreen configuration.


--------------------------------------------------------------------------------------------------------------------------------
RULES
--------------------------------------------------------------------------------------------------------------------------------
The rules of the game are as follows:
    Players start at the back wall of their team's side of the court and must stay on their team's side of the court.
    Until a ball has been put 'into play', it can only be picked up a by a player from the team whose colour matches the marker around it.
    A ball can not be thrown until it is put 'into play'.
    To put a ball 'into play' it must be picked up and carried to one of the back walls of the court.
    If a player holds a ball for more than 10 seconds, that ball will be surrendered to the other team.
    If a player is hit by a ball thrown by an opposing player, they are eliminated.
    If a player is holding a ball, they can use it to deflect other balls thrown by opposing Players.
    If a player catches a ball thrown by an opposing player, the opposing player is eliminated and the most recently eliminated player from the catching player's team (if any) returns to the game.
    A game ends when all players on one team have been eliminated, the team with at least one member remaining is declared the winner.


--------------------------------------------------------------------------------------------------------------------------------
CONTROLS
--------------------------------------------------------------------------------------------------------------------------------
The keyboard control schemes for the game are as follows:

Player 1
    Movement (Up/Left/Down/Right)   - W/A/S/D
    Dodge                           - Left Shift
    Pickup/Throw                    - Space
    Catch/deflect                   - Left Ctrl
    Pause                           - Escape

Player 2
    Movement (Up/Left/Down/Right)   - Up/Left/Down/Right
    Dodge                           - Right Shift
    Pickup/Throw                    - Right Ctrl
    Catch/deflect                   - Enter
    Pause                           - Pause


--------------------------------------------------------------------------------------------------------------------------------
POTENTIAL FUTURE IMPROVEMENTS
--------------------------------------------------------------------------------------------------------------------------------
--------Unique Characters--------
    It was originally planned that the player would be able to choose their avatar from a roster of different characters.
    Each of these different characters would have their own stats (hence why the 'CharacterStats' struct was created, rather than the use of hardcoded values).
    These stats would have governed things like movement speed, cooldown rates and throw speed.
    as well as having a special ability that would build up over the course of the game.
    Eventually only one character, the Knight, was included in the game (with no special ability) owing to the considerable amount of art assets that would need to created to support this feature.
    These include a character selection screen featuring representations of each character, in-game animations for all characters and UI to indicate the cooldown state for characters.

    The proposed characters and their associated special ability are detailed below:

    Knight      - Lets out a battle cry which makes their teammates move faster and throw balls harder for a brief duration
    Mage        - Thrown ball becomes an uncatchable fireball that zig zags through the enemy team's side of the court
    Maiden      - Lets out a cheer that makes their teammates invincible for a few seconds
    Archer      - Thrown ball splits into three, which fire in multiple directions, one of which persists upon hitting a wall to become a normal ball again
    Cleric      - Revives all eliminated teammates
    Bard        - Instantly fills all teammates special cooldowns    
    Dark Knight - All balls on the opponent's side of the court (including those held by opponents) are teleported the Dark Knight's side of the court
    Alchemist   - Freeze enemy players for a short time


--------Onslaught Mode--------
    This would have been an extra game mode in which the player (or two players cooperatively) face off against an endless stream of enemies.
    Each team would have had an endless supply of balls from a spawner on their side of the court.
    These balls, however, would destroy themselves upon hitting one of the back walls of the court, to avoid the screen becoming too cluttered.
    Every few seconds, a new enemy would have been added to the opposite side of the court.
    The player(s) would therefore have to be constantly eliminating enemies to keep from becoming overwhelmed, while at the same time avoiding balls thrown by their opponents.
    The rate at which new enemies spawned could have increased as time went on in order to create a difficulty curve.
    High scores for this mode would have been tracked and displayed upon conclusion of a session.


--------Gamepad Input--------
    A proposed gamepad control scheme is outlined below. Button names are given for the DualShock 4 and Xbox gamepads.
    Having a second analog stick would have allowed the player to aim a throw independently of the direction they were moving.
    This control scheme was not included in the game since there was not enough time to develop the UI assets required to explain its existence to the player.

    Movement (Up/Left/Down/Right)   - Left analog stick
    Aim                             - Right analog stick
    Dodge                           - Square/X
    Pickup/Throw                    - Cross/A
    Catch/deflect                   - Circle/B
    Pause                           - Options/start


--------AI Improvements--------
    Presently the AI routine can be summarised as:
		"If I've just picked up a Ball, I need to move to a random spot and then throw the Ball at an opposing Character"
		"If I'm not holding a Ball, I need to find one to pick up and move towards it"
		"If there are no Balls I can pick up, I will move to a random location while I wait for one to become available"

    Ideally, the AI would eventually include behaviours that allowed it to respond to balls being thrown around the court,
    perhaps having a random chance to dodge out of the way of, or deflecting, a ball if one is detected nearby.
    
    The AI was originally also planned to have a random chance of catching balls that were thrown near it, however this would have resulted
    in the player potentially being eliminated from the game based on random chance, which would make for a suboptimal play experience.


--------Keyboard Controlled Menus--------
    Currently all menus in the game must be navigated by clicking on items with the mouse.
    It would have been preferable for the user to have the option to use the keyboard to navigate these menus.
    This also would have been essential, had the gamepad input mentioned above also been implemented.
    However, this would have necessitated the creation of extra UI assets to indicate which option is currently selected, for which there was not time
